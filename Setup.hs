{-# LANGUAGE CPP #-}
import Distribution.Simple
import Distribution.Simple.LocalBuildInfo
import Distribution.Simple.Setup
import Distribution.PackageDescription
import Distribution.System
import System.IO.Error

import           Development.Lightening.Gen_Instructions
import qualified Data.ByteString.Char8 as B

main :: IO ()
main = defaultMainWithHooks simpleUserHooks { buildHook = myBuildHook }

myBuildHook :: PackageDescription -> LocalBuildInfo -> UserHooks -> BuildFlags -> IO ()
myBuildHook pkgDesc lbi hooks flags = do
    insns <- parse_header (is64 $ hostPlatform lbi) <$> readFile "lightening.h"
    maybeWriteFile "Development/Lightening/Instructions.hs" $ B.pack $ generate_instructions insns
    maybeWriteFile "lightening/wrappers.c" $ B.pack $ generate_wrappers insns

    buildHook simpleUserHooks pkgDesc lbi hooks flags
  where
#if MIN_VERSION_Cabal(2,4,0)
    is64 (Platform arch _os) = arch `elem` [ X86_64, PPC64, AArch64, IA64, Alpha ]
#else
    is64 (Platform arch _os) = arch `elem` [ X86_64, PPC64, IA64, Alpha ]
#endif

maybeWriteFile :: FilePath -> B.ByteString -> IO ()
maybeWriteFile fp new = do
    old <- tryIOError $ B.readFile fp
    if old == Right new
      then putStr $ fp ++ " is unchanged.\n"
      else B.writeFile fp new

