{-# LANGUAGE CPP #-}
module Tests ( tests ) where

#include "MachDeps.h"

import Control.Monad
import Data.Bits
import Data.Int
import Data.Word
import Development.Lightening
import Distribution.TestSuite
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Foreign.Storable

withArena :: (JitState -> IO () -> IO Result) -> IO Progress
withArena k = do
    has_jit <- init_jit
    if has_jit == 0
      then return $ Finished $ Error "init_jit failed"
      else do
        j <- jit_new_state
        if j == nullPtr
          then return $ Finished $ Error "jit_new_state failed"
          else do
            let arena_size = 4096
            newArena arena_size >>= \case
              Nothing -> return $ Finished $ Error "allocating JIT code buffer failed"
              Just arena_base -> do
                jit_begin j arena_base arena_size
                r <- k j (unlockArena arena_base arena_size)
                jit_destroy_state j
                freeArena arena_base arena_size
                return $ Finished r

infix 0 :=, :~
data TestCase where
    (:=) :: (Show a, Eq a) => IO a -> a -> TestCase
    (:~) :: (Show a, Eq a) => IO a -> (a -> Bool) -> TestCase

expect :: [ TestCase ] -> IO Result
expect = go (1::Int)
  where
    go _ [             ] = return Pass
    go n ( (f:~p) : ts ) = do
        a <- f
        if p a then go (n+1) ts
               else return $ Fail $ "(" ++ show n ++ "): " ++ show a
    go n ( (f:=b) : ts ) = do
        a <- f
        if a == b then go (n+1) ts
                  else return $ Fail $ "(" ++ show n ++ "): " ++ show a ++ " != " ++ show b


type Dimp a = FunPtr a -> a

foreign import ccall "dynamic" dyncall_P :: Dimp (IO (Ptr a))
foreign import ccall "dynamic" dyncall_I :: Dimp (IO Int)
foreign import ccall "dynamic" dyncall_II :: Dimp (Int -> IO Int)
foreign import ccall "dynamic" dyncall_III :: Dimp (Int -> Int -> IO Int)
foreign import ccall "dynamic" dyncall_U :: Dimp (IO Word)
foreign import ccall "dynamic" dyncall_UU :: Dimp (Word -> IO Word)
foreign import ccall "dynamic" dyncall_UUU :: Dimp (Word -> Word -> IO Word)

foreign import ccall "dynamic" dyncall_IPU :: Dimp (Ptr a -> Word -> IO Int)
foreign import ccall "dynamic" dyncall_DPU :: Dimp (Ptr a -> Word -> IO Double)
foreign import ccall "dynamic" dyncall_FPU :: Dimp (Ptr a -> Word -> IO Float)

foreign import ccall "dynamic" dyncall_UI :: Dimp (Int -> IO Word)

foreign import ccall "dynamic" dyncall_FI :: Dimp (Int -> IO Float)
foreign import ccall "dynamic" dyncall_IFF :: Dimp (Float -> Float -> IO Int)
foreign import ccall "dynamic" dyncall_DI :: Dimp (Int -> IO Double)
foreign import ccall "dynamic" dyncall_IDD :: Dimp (Double -> Double -> IO Int)
foreign import ccall "dynamic" dyncall_FD :: Dimp (Double -> IO Float)
foreign import ccall "dynamic" dyncall_DF :: Dimp (Float -> IO Double)

foreign import ccall "dynamic" dyncall_I32F :: Dimp (Float -> IO Int32)
foreign import ccall "dynamic" dyncall_I32D :: Dimp (Double -> IO Int32)

foreign import ccall "dynamic" dyncall_D :: Dimp (IO Double)
foreign import ccall "dynamic" dyncall_DD :: Dimp (Double -> IO Double)
foreign import ccall "dynamic" dyncall_DDD :: Dimp (Double -> Double -> IO Double)

foreign import ccall "dynamic" dyncall_F :: Dimp (IO Float)
foreign import ccall "dynamic" dyncall_FF :: Dimp (Float -> IO Float)
foreign import ccall "dynamic" dyncall_FFF :: Dimp (Float -> Float -> IO Float)

foreign import ccall "dynamic" dyncall_IP :: Dimp (Ptr a -> IO Int)
foreign import ccall "dynamic" dyncall_UP :: Dimp (Ptr a -> IO Word)
foreign import ccall "dynamic" dyncall_DP :: Dimp (Ptr a -> IO Double)
foreign import ccall "dynamic" dyncall_FP :: Dimp (Ptr a -> IO Float)
foreign import ccall "dynamic" dyncall_DU :: Dimp (Word -> IO Double)
foreign import ccall "dynamic" dyncall_FU :: Dimp (Word -> IO Float)

foreign import ccall "dynamic" dyncall_VF :: Dimp (Float -> IO ())
foreign import ccall "dynamic" dyncall_VD :: Dimp (Double -> IO ())
foreign import ccall "dynamic" dyncall_VI8 :: Dimp (Int8 -> IO ())
foreign import ccall "dynamic" dyncall_VI16 :: Dimp (Int16 -> IO ())
foreign import ccall "dynamic" dyncall_VI32 :: Dimp (Int32 -> IO ())

foreign import ccall "dynamic" dyncall_VIF :: Dimp (Int -> Float -> IO ())
foreign import ccall "dynamic" dyncall_VID :: Dimp (Int -> Double -> IO ())
foreign import ccall "dynamic" dyncall_VII8 :: Dimp (Int -> Int8 -> IO ())
foreign import ccall "dynamic" dyncall_VII16 :: Dimp (Int -> Int16 -> IO ())
foreign import ccall "dynamic" dyncall_VII32 :: Dimp (Int -> Int32 -> IO ())

foreign import ccall "dynamic" dyncall_VPF :: Dimp (Ptr a -> Float -> IO ())
foreign import ccall "dynamic" dyncall_VPD :: Dimp (Ptr a -> Double -> IO ())
foreign import ccall "dynamic" dyncall_VPI8 :: Dimp (Ptr a -> Int8 -> IO ())
foreign import ccall "dynamic" dyncall_VPI16 :: Dimp (Ptr a -> Int16 -> IO ())
foreign import ccall "dynamic" dyncall_VPI32 :: Dimp (Ptr a -> Int32 -> IO ())

foreign import ccall "dynamic" dyncall_VPIF :: Dimp (Ptr a -> Int -> Float -> IO ())
foreign import ccall "dynamic" dyncall_VPID :: Dimp (Ptr a -> Int -> Double -> IO ())
foreign import ccall "dynamic" dyncall_VPII8 :: Dimp (Ptr a -> Int -> Int8 -> IO ())
foreign import ccall "dynamic" dyncall_VPII16 :: Dimp (Ptr a -> Int -> Int16 -> IO ())
foreign import ccall "dynamic" dyncall_VPII32 :: Dimp (Ptr a -> Int -> Int32 -> IO ())

#if WORD_SIZE_IN_BITS > 32
foreign import ccall "dynamic" dyncall_I64F :: Dimp (Float -> IO Int64)
foreign import ccall "dynamic" dyncall_I64D :: Dimp (Double -> IO Int64)
foreign import ccall "dynamic" dyncall_VI64 :: Dimp (Int64 -> IO ())
foreign import ccall "dynamic" dyncall_VII64 :: Dimp (Int -> Int64 -> IO ())
foreign import ccall "dynamic" dyncall_VPI64 :: Dimp (Ptr a -> Int64 -> IO ())
foreign import ccall "dynamic" dyncall_VPII64 :: Dimp (Ptr a -> Int -> Int64 -> IO ())
#endif

foreign import ccall "dynamic" dyncall_VPPII :: Dimp (Ptr a -> Ptr b -> Int -> Int -> IO ())
foreign import ccall "dynamic" dyncall_VPPUU :: Dimp (Ptr a -> Ptr b -> Word -> Word -> IO ())

foreign import ccall "dynamic" dyncall_9 :: Dimp
    (Ptr a -> Int8 -> Int16 -> Int32 -> Int -> Word16 -> Float -> Double -> Float -> IO (Ptr a))

foreign import ccall unsafe "&call_10_test" call_10_test :: FunPtr
    (Int32 -> Int32 -> Int32 -> Int32 -> Int32 -> Int32 -> Int32 -> Int32 -> Int32 -> Int32 -> IO Int)

foreign import ccall unsafe "&tail_call_test" tail_call_test :: FunPtr (IO Int)

withJit :: (Jit () -> Jit Result) -> IO Progress
withJit k =
    withArena $ \j unlock ->
        runJit (k (io unlock)) (\r _ -> return r) j

tests :: IO [Test]
tests = return $
    let nm $$ fun = Test $ TestInstance { run = withArena fun
                                        , name = nm
                                        , tags = []
                                        , options = []
                                        , setOption = const . Left . (++) "don't know about " . show } in

    let nm $* fun = Test $ TestInstance { run = withJit fun
                                        , name = nm
                                        , tags = []
                                        , options = []
                                        , setOption = const . Left . (++) "don't know about " . show } in

 [ "absr_d" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiDouble jit_F0]

    absr_d jit_F0 jit_F0
    leave_jit_abi 0 0 align
    retr_d jit_F0

    f <- end dyncall_DD
    unlock
    io $ expect [ f   0.0  := 0.0
                , f (-0.0) := 0.0
                , f   0.5  := 0.5
                , f (-0.5) := 0.5 ]

 , "absr_f" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiFloat jit_F0]

    absr_f jit_F0 jit_F0
    leave_jit_abi 0 0 align
    retr_f jit_F0

    f <- end dyncall_FF
    unlock
    io $ expect [ f   0.0  := 0.0
                , f (-0.0) := 0.0
                , f   0.5  := 0.5
                , f (-0.5) := 0.5 ]

 , "addi" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    addi jit_R0 jit_R0 69
    leave_jit_abi 0 0 align
    retr jit_R0

    f <- end dyncall_II
    unlock
    io $ expect [ f 42 := 111 ]

 , "addr" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    addr jit_R0 jit_R0 jit_R1
    leave_jit_abi 0 0 align
    retr jit_R0

    f <- end dyncall_III
    unlock
    io $ expect [ f 42 69 := 111 ]

 , "addr_d" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiDouble jit_F0
              ,JitOperandFpr JitOperandAbiDouble jit_F1]

    addr_d jit_F0 jit_F0 jit_F1
    leave_jit_abi 0 0 align
    retr_d jit_F0

    f <- end dyncall_DDD
    unlock
    io $ expect [ f 42.0 69.0 := 111.0
                , f 42.5 69.5 := 112.0 ]

 , "addr_f" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiFloat jit_F0
              ,JitOperandFpr JitOperandAbiFloat jit_F1]

    addr_f jit_F0 jit_F0 jit_F1
    leave_jit_abi 0 0 align
    retr_f jit_F0

    f <- end dyncall_FFF
    unlock
    io $ expect [ f 42.0 69.0 := 111.0
                , f 42.5 69.5 := 112.0 ]

 , "addx" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    movi  jit_R2 0
    addcr jit_R0 jit_R0 jit_R1
    addxi jit_R2 jit_R2 0
    leave_jit_abi 0 0 align
    retr jit_R2

    f <- end dyncall_UUU
    unlock
    io . expect $ [ f 0 0 := 0 ]
#if WORD_SIZE_IN_BITS == 32
               ++ [ f 0xffffffff 0xffffffff := 1                    -- carry
                  , f 0x7fffffff 1 := 0                             -- overflow
                  , f 0x7fffffff 0x7fffffff := 0                    -- overflow
                  , f 0x7fffffff 0x80000000 := 0                    -- carry
                  , f 0x80000000 0x80000000 := 1 ]                  -- carry+overflow
#else
               ++ [ f 0xffffffff 0xffffffff := 0                    -- nothing
                  , f 0x7fffffff 1 := 0                             -- nothing
                  , f 0x7fffffff 0x7fffffff := 0                    -- nothing
                  , f 0x7fffffff 0x80000000 := 0                    -- nothing
                  , f 0x80000000 0x80000000 := 0                    -- nothing
                  , f 0xffffffffffffffff 0xffffffffffffffff := 1    -- carry
                  , f 0x7fffffffffffffff 1 := 0                     -- overflow
                  , f 0x7fffffffffffffff 0x7fffffffffffffff := 0    -- overflow
                  , f 0x7fffffffffffffff 0x8000000000000000 := 0    -- overflow
                  , f 0x8000000000000000 0x8000000000000000 := 1 ]  -- carry+overflow
#endif

 , "andi" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    andi jit_R0 jit_R0 1
    leave_jit_abi 0 0 align
    retr jit_R0

    f <- end dyncall_UU
    unlock
    io . expect $ [ f 0x7fffffff := 1
                , f 0x80000000 := 0 ]
#if WORD_SIZE_IN_BITS == 64
               ++ [ f 0x7fffffffffffffff := 1
                  , f 0x8000000000000000 := 0 ]
#endif

 , "andr" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    andr jit_R0 jit_R0 jit_R1
    leave_jit_abi 0 0 align
    retr jit_R0

    f <- end dyncall_UUU
    unlock
    io . expect $ [ f 0x7fffffff 1 := 1
                  , f 1 0x7fffffff := 1
                  , f 0x80000000 1 := 0
                  , f 1 0x80000000 := 0
                  , f 0x7fffffff 0x80000000 := 0
                  , f 0x80000000 0x7fffffff := 0
                  , f 0x7fffffff 0xffffffff := 0x7fffffff
                  , f 0xffffffff 0x7fffffff := 0x7fffffff
                  , f 0xffffffff 0xffffffff := 0xffffffff
                  , f 0x7fffffff 0 := 0
                  , f 0 0x7fffffff := 0 ]
#if WORD_SIZE_IN_BITS == 64
               ++ [ f 0x7fffffffffffffff 1 := 1
                  , f 1 0x7fffffffffffffff := 1
                  , f 0x8000000000000000 1 := 0
                  , f 1 0x8000000000000000 := 0
                  , f 0x7fffffffffffffff 0x8000000000000000 := 0
                  , f 0x8000000000000000 0x7fffffffffffffff := 0
                  , f 0x7fffffffffffffff 0xffffffffffffffff := 0x7fffffffffffffff
                  , f 0xffffffffffffffff 0x7fffffffffffffff := 0x7fffffffffffffff
                  , f 0xffffffffffffffff 0xffffffffffffffff := 0xffffffffffffffff ]
#endif

 , "beqi" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    r <- beqi jit_R0 0
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_II
    unlock
    io $ expect [ f   0  := 1
                , f   1  := 0
                , f (-1) := 0 ]

 , "beqr" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    r <- beqr jit_R0 jit_R1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_III
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 0
                , f (-1) 0 := 0
                , f 0 (-1) := 0
                , f 1 1 := 1 ]

 , "beqr_d" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiDouble jit_F0
              ,JitOperandFpr JitOperandAbiDouble jit_F1 ]

    r <- beqr_d jit_F0 jit_F1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_IDD
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 0
                , f (-1) 0 := 0
                , f 0 (-1) := 0
                , f 1 1 := 1
                , f 0 (0.0/0.0) := 0
                , f (0.0/0.0) 0 := 0 ]

 , "beqr_f" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiFloat jit_F0
              ,JitOperandFpr JitOperandAbiFloat jit_F1 ]

    r <- beqr_f jit_F0 jit_F1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_IFF
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 0
                , f (-1) 0 := 0
                , f 0 (-1) := 0
                , f 1 1 := 1
                , f 0 (0.0/0.0) := 0
                , f (0.0/0.0) 0 := 0 ]

 , "bgei" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    r <- bgei jit_R0 0
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_II
    unlock
    io $ expect [ f 0 := 1
                , f 1 := 1
                , f (-1) := 0 ]

 , "bgei_u" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    r <- bgei_u jit_R0 0
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_II
    unlock
    io $ expect [ f 0 := 1
                , f 1 := 1
                , f (-1) := 1 ]

 , "bger" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0 ]

    r <- bger jit_R0 jit_R1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_III
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 0
                , f 0 (-1) := 1 ]

 , "bger_d" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiDouble jit_F0
              ,JitOperandFpr JitOperandAbiDouble jit_F1]

    r <- bger_d jit_F0 jit_F1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_IDD
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 0
                , f 0 (-1) := 1
                , f 0 (0.0/0.0) := 0
                , f (0.0/0.0) 0 := 0 ]

 , "bger_f" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiFloat jit_F0
              ,JitOperandFpr JitOperandAbiFloat jit_F1]

    r <- bger_f jit_F0 jit_F1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_IFF
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 0
                , f 0 (-1) := 1
                , f 0 (0.0/0.0) := 0
                , f (0.0/0.0) 0 := 0 ]

 , "bger_u" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    r <- bger_u jit_R0 jit_R1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_III
    unlock
    io $ expect [ f 0 0 := 1
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 1
                , f 0 (-1) := 0 ]

 , "bgti" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    r <- bgti jit_R0 0
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_II
    unlock
    io $ expect [ f 0 := 0
                , f 1 := 1
                , f (-1) := 0 ]

 , "bgti_u" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    r <- bgti_u jit_R0 0
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_II
    unlock
    io $ expect [ f 0 := 0
                , f 1 := 1
                , f (-1) := 1 ]

 , "bgtr" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    r <- bgtr jit_R0 jit_R1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_III
    unlock
    io $ expect [ f 0 0 := 0
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 0
                , f 0 (-1) := 1 ]

 , "bgtr_r" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandFpr JitOperandAbiFloat jit_F0
              ,JitOperandFpr JitOperandAbiFloat jit_F1]

    r <- bgtr_f jit_F0 jit_F1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_IFF
    unlock
    io $ expect [ f 0 0 := 0
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 0
                , f 0 (-1) := 1
                , f 0 (0.0/0.0) := 0
                , f (0.0/0.0) 0 := 0 ]

 , "bgtr_u" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0
              ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    r <- bgtr_u jit_R0 jit_R1
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_III
    unlock
    io $ expect [ f 0 0 := 0
                , f 0 1 := 0
                , f 1 0 := 1
                , f (-1) 0 := 1
                , f 0 (-1) := 0 ]

 , "blei" $* \unlock -> do
    align <- enter_jit_abi 0 0 0
    load_args [JitOperandGpr jitOperandAbiWord jit_R0 0]

    r <- blei jit_R0 0
    leave_jit_abi 0 0 align
    reti 0
    patch_here r
    leave_jit_abi 0 0 align
    reti 1

    f <- end dyncall_II
    unlock
    io $ expect [ f 0 := 1
                , f 1 := 0
                , f (-1) := 1 ]

 , "blei_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_blei_u j r jit_R0 0
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 1
           , f 1 := 0
           , f (-1) := 0 ]

 , "bler" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bler j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0 ]

 , "bler_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bler_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0 ]

 , "bler_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bler_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0 ]

 , "bler_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bler_u j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 1 ]

 , "bltgtr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bltgtr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 0
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0
           , f (0.0/0.0) (0.0/0.0) := 0 ]

 , "bltgtr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bltgtr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 0
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0
           , f (0.0/0.0) (0.0/0.0) := 0 ]

 , "blti" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_blti j r jit_R0 0
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 0
           , f 1 := 0
           , f (-1) := 1 ]

 , "blti_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_blti_u j r jit_R0 0
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 0
           , f 1 := 0
           , f (-1) := 0 ]

 , "bltr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bltr j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0 ]

 , "bltr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bltr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0 ]

 , "bltr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bltr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0 ]

 , "bltr_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bltr_u j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 1 ]

 , "bmci" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bmci j r jit_R0 1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 1
           , f 1 := 0
           , f (-1) := 0
           , f 2 := 1 ]

 , "bmcr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bmcr j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 0
           , f 1 (-1) := 0
           , f (-1) 1 := 0
           , f (-1) (-1) := 0 ]

 , "bmsi" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bmsi j r jit_R0 1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f (-1) := 1
           , f 2 := 0 ]

 , "bmsr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bmsr j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 0
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 0
           , f 1 1 := 1
           , f 1 (-1) := 1
           , f (-1) 1 := 1
           , f (-1) (-1) := 1 ]

 , "bnei" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bnei j r jit_R0 0
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f (-1) := 1 ]

 , "bner" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bner j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_III
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1 ]

 , "bner_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bner_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1
           , f (0.0/0.0) (0.0/0.0) := 1 ]

 , "bner_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bner_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1
           , f (0.0/0.0) (0.0/0.0) := 1 ]


 , "boaddi" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_boaddi j r jit_R0 1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := 0
           , f 0 := 1
           , f 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff := overflowed
           , f 0x80000000 := 0x80000001
           , f 0xffffffff := 0 ]
#else
           , f 0x7fffffffffffffff := overflowed
           , f 0x8000000000000000 := 0x8000000000000001
           , f 0xffffffffffffffff := 0 ]
#endif

 , "boaddi_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_boaddi_u j r jit_R0 1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := overflowed
           , f 0 := 1
           , f 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff := 0x80000000
           , f 0x80000000 := 0x80000001
           , f 0xffffffff := overflowed ]
#else
           , f 0x7fffffffffffffff := 0x8000000000000000
           , f 0x8000000000000000 := 0x8000000000000001
           , f 0xffffffffffffffff := overflowed ]
#endif

 , "boaddr" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_boaddr j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 1 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := complement 1
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := overflowed
           , f 0x7fffffff 0x7fffffff := overflowed
           , f 0x7fffffff 0x80000000 := complement 0
           , f 0x80000000 0x80000000 := overflowed ]
#else
           , f 0xffffffff 0xffffffff := 0xffffffff + 0xffffffff
           , f 0x7fffffff 1 := 0x80000000
           , f 0x7fffffff 0x7fffffff := 0x7fffffff + 0x7fffffff
           , f 0x7fffffff 0x80000000 := 0xffffffff
           , f 0x80000000 0x80000000 := 0x100000000
           , f 0xffffffffffffffff 0xffffffffffffffff := complement 1
           , f 0x7fffffffffffffff 1 := overflowed
           , f 0x7fffffffffffffff 0x7fffffffffffffff := overflowed
           , f 0x7fffffffffffffff 0x8000000000000000 := complement 0
           , f 0x8000000000000000 0x8000000000000000 := overflowed ]
#endif

 , "boaddr_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_boaddr_u j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 1 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := overflowed
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := 0x80000000
           , f 0x7fffffff 0x7fffffff := complement 1
           , f 0x7fffffff 0x80000000 := complement 0
           , f 0x80000000 0x80000000 := overflowed ]
#else
           , f 0xffffffff 0xffffffff := 0xffffffff + 0xffffffff
           , f 0x7fffffff 1 := 0x80000000
           , f 0x7fffffff 0x7fffffff := 0x7fffffff + 0x7fffffff
           , f 0x7fffffff 0x80000000 := 0xffffffff
           , f 0x80000000 0x80000000 := 0x100000000
           , f 0xffffffffffffffff 0xffffffffffffffff := overflowed
           , f 0x7fffffffffffffff 1 := 0x8000000000000000
           , f 0x7fffffffffffffff 0x7fffffffffffffff := complement 1
           , f 0x7fffffffffffffff 0x8000000000000000 := complement 0
           , f 0x8000000000000000 0x8000000000000000 := overflowed ]
#endif

 , "bordr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bordr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 1
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0
           , f (0.0/0.0) (0.0/0.0) := 0 ]

 , "bordr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bordr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 1
           , f (-1) 0 := 1
           , f 0 (-1) := 1
           , f 1 1 := 1
           , f 0 (0.0/0.0) := 0
           , f (0.0/0.0) 0 := 0
           , f (0.0/0.0) (0.0/0.0) := 0 ]

 , "bosubi" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bosubi j r jit_R0 1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := complement 1
           , f 0 := complement 0
           , f 1 := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff := 0x7ffffffe
           , f 0x80000000 := overflowed
           , f 0x80000001 := 0x80000000 ]
#else
           , f 0x7fffffffffffffff := 0x7ffffffffffffffe
           , f 0x8000000000000000 := overflowed
           , f 0x8000000000000001 := 0x8000000000000000 ]
#endif

 , "bosubi_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bosubi_u j r jit_R0 1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock

    expect [ f (complement 0) := complement 1
           , f 0 := overflowed
           , f 1 := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x80000000 := 0x7fffffff ]
#else
           , f 0x8000000000000000 := 0x7fffffffffffffff ]
#endif

 , "bosubr" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bosubr j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := complement 0
           , f 1 1 := 0
           , f 1 (complement 0) := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := 0
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := 0x7ffffffe
           , f 0x7fffffff 0x7fffffff := 0
           , f 0x80000000 0x7fffffff := overflowed
           , f 0x7fffffff 0x80000000 := overflowed
           , f 0x80000000 0x80000000 := 0 ]
#else
           , f 0x7fffffffffffffff 0x7fffffffffffffff := 0
           , f 0x7fffffffffffffff 0x8000000000000000 := overflowed
           , f 0x8000000000000000 0x7fffffffffffffff := overflowed
           , f 0x8000000000000000 0x8000000000000000 := 0 ]
#endif

 , "bosubr_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bosubr_u j r jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0
        jit_patch_here j r
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 1 1 := 0
           , f 0 1 := overflowed
           , f 1 0 := 1
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := 0
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := 0x7ffffffe
           , f 0x7fffffff 0x7fffffff := 0
           , f 0x7fffffff 0x80000000 := overflowed
           , f 0x80000000 0x80000000 := 0 ]
#else
           , f 0xffffffffffffffff 0xffffffffffffffff := 0
           , f 0x7fffffffffffffff 0x7fffffffffffffff := 0
           , f 0x7fffffffffffffff 0x8000000000000000 := overflowed
           , f 0x8000000000000000 0x8000000000000000 := 0 ]
#endif

 , "bswapr_ui" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_bswapr_ui j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _size) <- jit_end j dyncall_UU
    unlock

    expect $ [ f 0 := 0
             , f 0x12345678 := 0x78563412 ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0xff12345678 := 0x78563412
             , f 0xff00000000 := 0 ]
#endif

#if WORD_SIZE_IN_BITS > 32
 , "bswapr_ul" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_bswapr_ul j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _size) <- jit_end j dyncall_UU
    unlock

    expect [ f 0 := 0
           , f 0x12345678 := 0x7856341200000000
           , f 0xff12345678 := 0x78563412ff000000 ]
#endif

 , "bswapr_us" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_bswapr_us j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _size) <- jit_end j dyncall_UU
    unlock

    expect [ f 0 := 0
           , f 0x12345678 := 0x7856 ]

 , "buneqr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_buneqr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 0
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 0
           , f 1 1 := 1
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "buneqr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_buneqr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 0
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 0
           , f 1 1 := 1
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunger_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bunger_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 0
           , f 1 0 := 1
           , f (-1) 0 := 0
           , f 0 (-1) := 1
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunger_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bunger_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 0
           , f 1 0 := 1
           , f (-1) 0 := 0
           , f 0 (-1) := 1
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bungtr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bungtr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 0
           , f 1 0 := 1
           , f (-1) 0 := 0
           , f 0 (-1) := 1
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bungtr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bungtr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 0
           , f 1 0 := 1
           , f (-1) 0 := 0
           , f 0 (-1) := 1
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunler_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bunler_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunler_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bunler_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 1
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunltr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bunltr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunltr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bunltr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 1
           , f 1 0 := 0
           , f (-1) 0 := 1
           , f 0 (-1) := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1 ]

 , "bunordr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    withReloc $ \r -> do
        jit_bunordr_d j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IDD
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 0
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 0
           , f 1 1 := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1
           , f (0.0/0.0) (0.0/0.0) := 1 ]

 , "bunordr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    withReloc $ \r -> do
        jit_bunordr_f j r jit_F0 jit_F1
        jit_leave_jit_abi j 0 0 align
        jit_reti j 0
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_reti j 1

    (f,_) <- jit_end j dyncall_IFF
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := 0
           , f 1 0 := 0
           , f (-1) 0 := 0
           , f 0 (-1) := 0
           , f 1 1 := 0
           , f 0 (0.0/0.0) := 1
           , f (0.0/0.0) 0 := 1
           , f (0.0/0.0) (0.0/0.0) := 1 ]

 , "bxaddi" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bxaddi j r jit_R0 1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := 0
           , f 0 := 1
           , f 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff := overflowed
           , f 0x80000000 := 0x80000001
           , f 0xffffffff := 0 ]
#else
           , f 0x7fffffffffffffff := overflowed
           , f 0x8000000000000000 := 0x8000000000000001
           , f 0xffffffffffffffff := 0 ]
#endif

 , "bxaddi_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bxaddi_u j r jit_R0 1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := overflowed
           , f 0 := 1
           , f 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff := 0x80000000
           , f 0x80000000 := 0x80000001
           , f 0xffffffff := overflowed ]
#else
           , f 0x7fffffffffffffff := 0x8000000000000000
           , f 0x8000000000000000 := 0x8000000000000001
           , f 0xffffffffffffffff := overflowed ]
#endif

 , "bxaddr" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bxaddr j r jit_R0 jit_R1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 1 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := complement 1
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := overflowed
           , f 0x7fffffff 0x7fffffff := overflowed
           , f 0x7fffffff 0x80000000 := complement 0
           , f 0x80000000 0x80000000 := overflowed ]
#else
           , f 0xffffffff 0xffffffff := 0xffffffff + 0xffffffff
           , f 0x7fffffff 1 := 0x80000000
           , f 0x7fffffff 0x7fffffff := 0x7fffffff + 0x7fffffff
           , f 0x7fffffff 0x80000000 := 0xffffffff
           , f 0x80000000 0x80000000 := 0x100000000
           , f 0xffffffffffffffff 0xffffffffffffffff := complement 1
           , f 0x7fffffffffffffff 1 := overflowed
           , f 0x7fffffffffffffff 0x7fffffffffffffff := overflowed
           , f 0x7fffffffffffffff 0x8000000000000000 := complement 0
           , f 0x8000000000000000 0x8000000000000000 := overflowed ]
#endif

 , "bxaddr_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bxaddr_u j r jit_R0 jit_R1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 1 1 := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := overflowed
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := 0x80000000
           , f 0x7fffffff 0x7fffffff := 0xfffffffe
           , f 0x7fffffff 0x80000000 := 0xffffffff
           , f 0x80000000 0x80000000 := overflowed ]
#else
           , f 0xffffffff 0xffffffff := 0xffffffff + 0xffffffff
           , f 0x7fffffff 1 := 0x80000000
           , f 0x7fffffff 0x7fffffff := 0xfffffffe
           , f 0x7fffffff 0x80000000 := 0xffffffff
           , f 0x80000000 0x80000000 := 0x100000000
           , f 0xffffffffffffffff 0xffffffffffffffff := overflowed
           , f 0x7fffffffffffffff 1 := 0x8000000000000000
           , f 0x7fffffffffffffff 0x7fffffffffffffff := complement 1
           , f 0x7fffffffffffffff 0x8000000000000000 := complement 0
           , f 0x8000000000000000 0x8000000000000000 := overflowed ]
#endif

 , "bxsubi" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bxsubi j r jit_R0 1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := complement 1
           , f 0 := complement 0
           , f 1 := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff := 0x7ffffffe
           , f 0x80000000 := overflowed
           , f 0x80000001 := 0x80000000 ]
#else
           , f 0x7fffffffffffffff := 0x7ffffffffffffffe
           , f 0x8000000000000000 := overflowed
           , f 0x8000000000000001 := 0x8000000000000000 ]
#endif

 , "bxsubi_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    withReloc $ \r -> do
        jit_bxsubi_u j r jit_R0 1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f (complement 0) := complement 1
           , f 0 := overflowed
           , f 1 := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x80000000 := 0x7fffffff ]
#else
           , f 0x8000000000000000 := 0x7fffffffffffffff ]
#endif

 , "bxsubr" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bxsubr j r jit_R0 jit_R1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 0 1 := complement 0
           , f 1 1 := 0
           , f 1 (complement 0) := 2
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := 0
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := 0x7ffffffe
           , f 0x7fffffff 0x7fffffff := 0
           , f 0x80000000 0x7fffffff := overflowed
           , f 0x7fffffff 0x80000000 := overflowed
           , f 0x80000000 0x80000000 := 0 ]
#else
           , f 0x7fffffffffffffff 0x7fffffffffffffff := 0
           , f 0x7fffffffffffffff 0x8000000000000000 := overflowed
           , f 0x8000000000000000 0x7fffffffffffffff := overflowed
           , f 0x8000000000000000 0x8000000000000000 := 0 ]
#endif

 , "bxsubr_u" $$ \j unlock -> do
    let overflowed = 0xcabba9e5
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    withReloc $ \r -> do
        jit_bxsubr_u j r jit_R0 jit_R1
        jit_movi j jit_R0 (fromIntegral overflowed)
        jit_patch_here j r
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
           , f 1 1 := 0
           , f 0 1 := overflowed
           , f 1 0 := 1
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff 0xffffffff := 0
           , f 0x7fffffff 0 := 0x7fffffff
           , f 0x7fffffff 1 := 0x7ffffffe
           , f 0x7fffffff 0x7fffffff := 0
           , f 0x7fffffff 0x80000000 := overflowed
           , f 0x80000000 0x80000000 := 0 ]
#else
           , f 0xffffffffffffffff 0xffffffffffffffff := 0
           , f 0x7fffffffffffffff 0x7fffffffffffffff := 0
           , f 0x7fffffffffffffff 0x8000000000000000 := overflowed
           , f 0x8000000000000000 0x8000000000000000 := 0 ]
#endif

 , "call_10" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0]
    jit_calli j call_10_test [ JitOperandMem JitOperandAbiInt32 jit_R0 (i * 4) 0 | i <- [0..9] ]
    jit_leave_jit_abi j 0 0 align
    jit_ret j

    (f, _) <- jit_end j dyncall_IP
    unlock
    expect [ withArray [0..9::Int32] f := 330 ]

 , "callee_9" $$ \j unlock -> do
    {- struct args
       {
         int8_t a;         // 0
         int16_t b;        // 2
         int32_t c;        // 4
         jit_word_t d;     // 8
         uint16_t e;       // 16
         float f;          // 20
         double g;         // 24
         float h;          // 32
       };          // size == 40
    -}
    allocaBytes 40 $ \args -> do
        align <- jit_enter_jit_abi j 3 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr JitOperandAbiInt8    jit_R1 0
                        ,JitOperandGpr JitOperandAbiInt16   jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt32   jit_V0 0
                        ,JitOperandGpr jitOperandAbiWord    jit_V1 0
                        ,JitOperandGpr JitOperandAbiUint16  jit_V2 0
                        ,JitOperandFpr JitOperandAbiFloat   jit_F0
                        ,JitOperandFpr JitOperandAbiDouble  jit_F1
                        ,JitOperandFpr JitOperandAbiFloat   jit_F2 ]

        jit_stxi_c j  0 jit_R0 jit_R1
        jit_stxi_s j  2 jit_R0 jit_R2
        jit_stxi_i j  4 jit_R0 jit_V0
        jit_stxi   j  8 jit_R0 jit_V1
        jit_stxi_s j 16 jit_R0 jit_V2
        jit_stxi_f j 20 jit_R0 jit_F0
        jit_stxi_d j 24 jit_R0 jit_F1
        jit_stxi_f j 32 jit_R0 jit_F2

        jit_leave_jit_abi j 3 0 align
        jit_retr j jit_R0

        (f, _) <- jit_end j dyncall_9
        unlock

        let peek_args p = (,,,,,,,,) p <$> (peekByteOff p 0 :: IO Int8)
                                       <*> (peekByteOff p 2 :: IO Int16)
                                       <*> (peekByteOff p 4 :: IO Int32)
                                       <*> (peekByteOff p 8 :: IO Int)
                                       <*> (peekByteOff p 16 :: IO Word16)
                                       <*> (peekByteOff p 20 :: IO Float)
                                       <*> (peekByteOff p 24 :: IO Double)
                                       <*> (peekByteOff p 32 :: IO Float)

        expect [ f args 0 1 2 3 4 5 6 7 >>= peek_args := (args,0,1,2,3,4,5,6,7) ]

 , "comr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_comr j jit_R0 jit_R0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UU
    unlock

    expect
#if WORD_SIZE_IN_BITS == 32
        [ f 0 := 0xffffffff
        , f 1 := 0xfffffffe
        , f 0xffffffff := 0
        , f 0x80000000 := 0x7fffffff
        , f 0x7fffffff := 0x80000000
        , f 0x80000001 := 0x7ffffffe ]
#else
        [ f 0 := 0xffffffffffffffff
        , f 1 := 0xfffffffffffffffe
        , f 0xffffffff := 0xffffffff00000000
        , f 0x80000000 := 0xffffffff7fffffff
        , f 0x7fffffff := 0xffffffff80000000
        , f 0x80000001 := 0xffffffff7ffffffe
        , f 0xffffffffffffffff := 0
        , f 0x8000000000000000 := 0x7fffffffffffffff
        , f 0x7fffffffffffffff := 0x8000000000000000
        , f 0x8000000000000001 := 0x7ffffffffffffffe ]
#endif

 , "divr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_divr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock

    expect $ [ f 0x7fffffff 1 := 0x7fffffff
             , f 1 0x7fffffff := 0
             , f 0x80000000 1 := 0x80000000
             , f 1 0x80000000 := 0
             , f 0x7fffffff 2 := 0x3fffffff
             , f 2 0x7fffffff := 0
             , f 2 0x80000000 := 0
             , f 0x7fffffff 0x80000000 := 0
             , f 0 0x7fffffff := 0
             , f 0xffffffff 0xffffffff := 1 ]
#if WORD_SIZE_IN_BITS == 32
          ++ [ f 0x80000000 2 := 0xc0000000
             , f 0x80000000 0x7fffffff := 0xffffffff
             , f 0x7fffffff 0xffffffff := 0x80000001
             , f 0xffffffff 0x7fffffff := 0 ]
#else
          ++ [ f 0x80000000 2 := 0x40000000
             , f 0x80000000 0x7fffffff := 1
             , f 0x7fffffff 0xffffffff := 0
             , f 0xffffffff 0x7fffffff := 2
             , f 0x7fffffffffffffff 1 := 0x7fffffffffffffff
             , f 1 0x7fffffffffffffff := 0
             , f 0x8000000000000000 1 := 0x8000000000000000
             , f 1 0x8000000000000000 := 0
             , f 0x7fffffffffffffff 2 := 0x3fffffffffffffff
             , f 2 0x7fffffffffffffff := 0
             , f 0x8000000000000000 2 := 0xc000000000000000
             , f 2 0x8000000000000000 := 0
             , f 0x7fffffffffffffff 0x8000000000000000 := 0
             , f 0x8000000000000000 0x7fffffffffffffff := 0xffffffffffffffff
             , f 0x7fffffffffffffff 0xffffffffffffffff := 0x8000000000000001
             , f 0xffffffffffffffff 0x7fffffffffffffff := 0
             , f 0xffffffffffffffff 0xffffffffffffffff := 1 ]
#endif

 , "divr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    jit_divr_d j jit_F0 jit_F0 jit_F1
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f, _) <- jit_end j dyncall_DDD
    unlock
    expect [ f (-0.5) 0.5 := -1.0
           , f  1.25  0.5 :=  2.5 ]

 , "divr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    jit_divr_f j jit_F0 jit_F0 jit_F1
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f, _) <- jit_end j dyncall_FFF
    unlock
    expect [ f (-0.5) 0.5 := -1.0
           , f  1.25  0.5 :=  2.5 ]

 , "divr_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_divr_u j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect $ [ f 0x7fffffff 1 := 0x7fffffff
             , f 1 0x7fffffff := 0
             , f 0x80000000 1 := 0x80000000
             , f 1 0x80000000 := 0
             , f 0x7fffffff 2 := 0x3fffffff
             , f 2 0x7fffffff := 0
             , f 0x80000000 2 := 0x40000000
             , f 2 0x80000000 := 0
             , f 0x7fffffff 0x80000000 := 0
             , f 0x80000000 0x7fffffff := 1
             , f 0 0x7fffffff := 0
             , f 0x7fffffff 0xffffffff := 0
             , f 0xffffffff 0x7fffffff := 2
             , f 0xffffffff 0xffffffff := 1 ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0x7fffffffffffffff 1 := 0x7fffffffffffffff
             , f 1 0x7fffffffffffffff := 0
             , f 0x8000000000000000 1 := 0x8000000000000000
             , f 1 0x8000000000000000 := 0
             , f 0x7fffffffffffffff 2 := 0x3fffffffffffffff
             , f 2 0x7fffffffffffffff := 0
             , f 0x8000000000000000 2 := 0x4000000000000000
             , f 2 0x8000000000000000 := 0
             , f 0x7fffffffffffffff 0x8000000000000000 := 0
             , f 0x8000000000000000 0x7fffffffffffffff := 1
             , f 0x7fffffffffffffff 0xffffffffffffffff := 0
             , f 0xffffffffffffffff 0x7fffffffffffffff := 2
             , f 0xffffffffffffffff 0xffffffffffffffff := 1 ]
#endif

 , "extr_c" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_extr_c j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f 0xf := 0xf
           , f 0xff := -1
           , f 0xfff := -1 ]

 , "extr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_extr_d j jit_F0 jit_R0
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f, _) <- jit_end j dyncall_DI
    unlock

    expect [ f 0 := 0.0
           , f 1 := 1.0
           , f (-100) := -100.0 ]

 , "extr_d_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

    jit_extr_d_f j jit_F0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f,_) <- jit_end j dyncall_FD
    unlock
    expect [ f 0.0 := 0.0
           , f 0.5 := 0.5
           , f (1.0 / 0.0) := 1.0 / 0.0
           , f 1.25 := 1.25 ]

 , "extr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_extr_f j jit_F0 jit_R0
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f,_) <- jit_end j dyncall_FI
    unlock
    expect [ f 0 := 0.0
           , f 1 := 1.0
           , f (-100) := -100.0 ]

 , "extr_f_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0]

    jit_extr_f_d j jit_F0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f,_) <- jit_end j dyncall_DF
    unlock
    expect [ f 0.0 := 0.0
           , f 0.5 := 0.5
           , f (1.0 / 0.0) := 1.0 / 0.0
           , f 1.25 := 1.25 ]

#if WORD_SIZE_IN_BITS > 32
 , "extr_i" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_extr_i j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f 0xfffffff := 0xfffffff
           , f 0xffffffff := complement 0
           , f 0xfffffffff := complement 0
           , f 0xf00000000 := 0 ]
#endif

 , "extr_s" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_extr_s j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_II
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f 0xfff := 0xfff
           , f 0xffff := -1
           , f 0xfffff := -1
           , f 0xf0000 := 0 ]

 , "extr_uc" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_extr_uc j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f 0xff := 0xff
           , f 0xfff := 0xff
           , f 0xf00 := 0 ]

#if WORD_SIZE_IN_BITS > 32
 , "extr_ui" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_extr_ui j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f 0xffffffff := 0xffffffff
           , f 0xfffffffff := 0xffffffff
           , f 0xf00000000 := 0 ]
#endif

 , "extr_us" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_extr_us j jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f 0 := 0
           , f 1 := 1
           , f 0xffff := 0xffff
           , f 0xfffff := 0xffff
           , f 0xf0000 := 0 ]

 , "jmpi" $$ \j unlock -> do
    jit_jmpi j (castFunPtrToPtr tail_call_test)
    (f,_) <- jit_end j dyncall_I
    unlock
    expect [ f := 42 ]

 , "jmpr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0]
    jit_leave_jit_abi j 0 0 align
    jit_jmpr j jit_R0
    (f,_) <- jit_end j dyncall_IP
    unlock
    expect [ f (castFunPtrToPtr tail_call_test) := 42 ]

 , "ldi_c" $$ \j unlock ->
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_c j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_I
        unlock
        expect [ f := -1 ]

 , "ldi_d" $$ \j unlock ->
    with (-1.5 :: Double) $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_d j jit_F0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr_d j jit_F0

        (f,_) <- jit_end j dyncall_D
        unlock
        expect [ f := -1.5 ]

 , "ldi_f" $$ \j unlock ->
    with (-1.5 :: Float) $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_f j jit_F0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr_f j jit_F0

        (f,_) <- jit_end j dyncall_F
        unlock
        expect [ f := -1.5 ]

 , "ldi_i" $$ \j unlock ->
    with (0xffffffff :: Word32) $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_i j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_I
        unlock
        expect [ f := -1 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldi_l" $$ \j unlock ->
    with (0xffffffffffffffff :: Word64) $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_l j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_I
        unlock
        expect [ f := -1 ]
#endif

 , "ldi_s" $$ \j unlock ->
    with (0xFFFF :: Word16) $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_s j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_I
        unlock
        expect [ f := -1 ]

 , "ldi_uc" $$ \j unlock ->
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_uc j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_U
        unlock
        expect [ f := 0xff ]

#if WORD_SIZE_IN_BITS > 32
 , "ldi_ui" $$ \j unlock ->
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_ui j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_U
        unlock
        expect [ f := 0xffffffff ]
#endif

 , "ldi_us" $$ \j unlock ->
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0

        jit_ldi_us j jit_R0 dat
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_U
        unlock
        expect [ f := 0xffff ]

 , "ldr_c" $$ \j unlock ->
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_c j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IP
        unlock
        expect [ f (advancePtr dat 0) := -1
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x42 ]

 , "ldr_d" $$ \j unlock ->
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_d j jit_F0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr_d j jit_F0

        (f,_) <- jit_end j dyncall_DP
        unlock
        expect [ f (advancePtr dat 0) := -1.0
               , f (advancePtr dat 1) := 0.0
               , f (advancePtr dat 2) := 0.5 ]

 , "ldr_f" $$ \j unlock ->
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_f j jit_F0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr_f j jit_F0

        (f,_) <- jit_end j dyncall_FP
        unlock
        expect [ f (advancePtr dat 0) := -1.0
               , f (advancePtr dat 1) := 0.0
               , f (advancePtr dat 2) := 0.5 ]

 , "ldr_i" $$ \j unlock ->
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_i j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IP
        unlock
        expect [ f (advancePtr dat 0) := -1
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x42424242 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldr_l" $$ \j unlock ->
    withArray [ 0xffffffffffffffff, 0, 0x4242424212345678 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_l j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IP
        unlock
        expect [ f (advancePtr dat 0) := -1
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x4242424212345678 ]
#endif

 , "ldr_s" $$ \j unlock ->
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_s j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IP
        unlock
        expect [ f (advancePtr dat 0) := -1
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x4242 ]

 , "ldr_uc" $$ \j unlock ->
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_uc j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UP
        unlock
        expect [ f (advancePtr dat 0) := 0xff
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x42 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldr_ui" $$ \j unlock ->
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_ui j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UP
        unlock
        expect [ f (advancePtr dat 0) := 0xffffffff
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x42424242 ]
#endif

 , "ldr_us" $$ \j unlock ->
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R1 0]

        jit_ldr_us j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UP
        unlock
        expect [ f (advancePtr dat 0) := 0xffff
               , f (advancePtr dat 1) := 0
               , f (advancePtr dat 2) := 0x4242 ]

 , "ldxi_c" $$ \j unlock ->
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_c j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_II
        unlock

        expect [ f 0 := -1
               , f 1 := 0
               , f 2 := 0x42 ]

 , "ldxi_d" $$ \j unlock ->
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_d j jit_F0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr_d j jit_F0

        (f,_) <- jit_end j dyncall_DU
        unlock
        expect [ f 0 := -1.0
               , f 8 := 0.0
               , f 16 := 0.5 ]

 , "ldxi_f" $$ \j unlock ->
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_f j jit_F0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr_f j jit_F0

        (f,_) <- jit_end j dyncall_FU
        unlock
        expect [ f 0 := -1.0
               , f 4 := 0.0
               , f 8 := 0.5 ]

 , "ldxi_i" $$ \j unlock ->
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0]

        jit_ldxi_i j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UU
        unlock
        expect [ f 0 := complement 0
               , f 4 := 0
               , f 8 := 0x42424242 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldxi_l" $$ \j unlock ->
    withArray [ 0xffffffffffffffff, 0, 0x4242424212345678 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_l j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UU
        unlock
        expect [ f 0 := complement 0
               , f 8 := 0
               , f 16 := 0x4242424212345678 ]
#endif

 , "ldxi_s" $$ \j unlock -> do
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_s j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UU
        unlock
        expect [ f 0 := complement 0
               , f 2 := 0
               , f 4 := 0x4242 ]

 , "ldxi_uc" $$ \j unlock -> do
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_uc j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UU
        unlock
        expect [ f 0 := 0xff
               , f 1 := 0
               , f 2 := 0x42 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldxi_ui" $$ \j unlock -> do
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_ui j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UU
        unlock
        expect [ f 0 := 0xffffffff
               , f 4 := 0
               , f 8 := 0x42424242 ]
#endif

 , "ldxi_us" $$ \j unlock -> do
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

        jit_ldxi_us j jit_R0 jit_R0 (fromIntegral $ ptrToIntPtr dat)
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_UU
        unlock
        expect [ f 0 := 0xffff
               , f 2 := 0
               , f 4 := 0x4242 ]

 , "ldxr_c" $$ \j unlock -> do
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_c j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock

        expect [ f dat 0 := -1
               , f dat 1 := 0
               , f dat 2 := 0x42 ]

 , "ldxr_d" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_d j jit_F0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr_d j jit_F0

        (f,_) <- jit_end j dyncall_DPU
        unlock
        expect [ f dat 0 := -1.0
               , f dat 8 := 0.0
               , f dat 16 := 0.5 ]

 , "ldxr_f" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_f j jit_F0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr_f j jit_F0

        (f,_) <- jit_end j dyncall_FPU
        unlock
        expect [ f dat 0 := -1.0
               , f dat 4 := 0.0
               , f dat 8 := 0.5 ]

 , "ldxr_i" $$ \j unlock -> do
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_i j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock
        expect [ f dat 0 := -1
               , f dat 4 := 0
               , f dat 8 := 0x42424242 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldxr_l" $$ \j unlock -> do
    withArray [ 0xffffffffffffffff, 0, 0x4242424212345678 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_l j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock
        expect [ f dat 0 := -1
               , f dat 8 := 0
               , f dat 16 := 0x4242424212345678 ]
#endif

 , "ldxr_s" $$ \j unlock -> do
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_s j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock
        expect [ f dat 0 := -1
               , f dat 2 := 0
               , f dat 4 := 0x4242 ]

 , "ldxr_uc" $$ \j unlock -> do
    withArray [ 0xff, 0x00, 0x42 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_uc j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock
        expect [ f dat 0 := 0xff
               , f dat 1 := 0
               , f dat 2 := 0x42 ]

#if WORD_SIZE_IN_BITS > 32
 , "ldxr_ui" $$ \j unlock -> do
    withArray [ 0xffffffff, 0x00000000, 0x42424242 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_ui j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock
        expect [ f dat 0 := 0xffffffff
               , f dat 4 := 0x00000000
               , f dat 8 := 0x42424242 ]
#endif

 , "ldxr_us" $$ \j unlock -> do
    withArray [ 0xffff, 0x0000, 0x4242 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R1 0]

        jit_ldxr_us j jit_R0 jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_IPU
        unlock
        expect [ f dat 0 := 0xffff
               , f dat 2 := 0x0000
               , f dat 4 := 0x4242 ]

 , "link-register" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0]

    withReloc $ \call_tramp -> do
        jit_jmp j call_tramp
        tramp <- jit_address j
        jit_pop_link_register j
        jit_movr j jit_R0 jit_LR
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        jit_patch_here j call_tramp
        jit_jmpi_with_link j tramp

    expected_link <- jit_address_to_function_pointer =<< jit_address j
    (f, _) <- jit_end j dyncall_P
    unlock
    expect [ f := castFunPtrToPtr expected_link ]

 , "lshi" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_lshi j jit_R0 jit_R0 31
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UI
    unlock
#if WORD_SIZE_IN_BITS == 32
    expect [ f (-0x7f) := 0x80000000 ]
#else
    expect [ f (-0x7f) := 0xffffffc080000000 ]
#endif

 , "lshr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_lshr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0x7f 1 := 0xfe
           , f 0x7fff 2 := 0x1fffc
           , f 0x81 16 := 0x810000
           , f 0xff 15 := 0x7f8000
           , f 0x7fffffff 0 := 0x7fffffff
#if WORD_SIZE_IN_BITS == 32
           , f 0xffffffff  8 := 0xffffff00
           , f 0x7fffffff  3 := 0xfffffff8
           , f 0x7fffff81 31 := 0x80000000
           , f 0x7fff8001 30 := 0x40000000
           , f 0x80000001 29 := 0x20000000
           , f 0x80000001 28 := 0x10000000
           , f 0x00008001 17 := 0x00020000
           , f 0x80000001 18 := 0x00040000
           , f 0xffff0001 24 := 0x01000000 ]
#else
           , f 0xffffffff 8 := 0xffffffff00
           , f 0x7fffffff 3 := 0x3fffffff8
           , f 0xffffffffffffff81 31 := 0xffffffc080000000
           , f 0xffffffffffff8001 30 := 0xffffe00040000000
           , f 0xffffffff80000001 29 := 0xf000000020000000
           , f 0x80000001 28 := 0x800000010000000
           , f 0x8001 17 := 0x100020000
           , f 0x80000001 18 := 0x2000000040000
           , f 0xffffffffffff0001 24 := 0xffffff0001000000
           , f 0x7f 33 := 0xfe00000000
           , f 0x7ffff 34 := 0x1ffffc00000000
           , f 0x7fffffff 35 := 0xfffffff800000000
           , f 0xffffffffffffff81 63 := 0x8000000000000000
           , f 0xffffffffffff8001 62 := 0x4000000000000000
           , f 0xffffffff80000001 61 := 0x2000000000000000
           , f 0x80000001 60 := 0x1000000000000000
           , f 0x81 48 := 0x81000000000000
           , f 0x8001 49 := 0x2000000000000
           , f 0x80000001 40 := 0x10000000000
           , f 0xff 47 := 0x7f800000000000
           , f 0xffff0001 56 := 0x100000000000000
           , f 0xffffffff 40 := 0xffffff0000000000
           , f 0x7fffffffff 33 := 0xfffffffe00000000
           , f 0xffffff8000000001 63 := 0x8000000000000000
           , f 0x8000000001 48 := 0x1000000000000
           , f 0xffffffffff 47 := 0xffff800000000000 ]
#endif

 , "mov_addr" $$ \j unlock -> do
    let thing = 0x123456789abcdef0 :: Word64
    with thing $ \pthing -> do
      withReloc $ \r -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_mov_addr j r jit_R0
        jit_patch_there j r pthing
        jit_leave_jit_abi j 0 0 align
        jit_retr j jit_R0

        (f,_) <- jit_end j dyncall_P
        unlock
        expect [ f := pthing
               , f >>= peek := thing ]

 , "movi_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0

    jit_movi_d j jit_F0 3.14159
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f,_) <- jit_end j dyncall_D
    unlock
    expect [ f := 3.14159 ]

 , "movi_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0

    jit_movi_f j jit_F0 3.14159
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f,_) <- jit_end j dyncall_F
    unlock
    expect [ f := 3.14159 ]

 , "mulr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_mulr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0x7fffffff 1 := 0x7fffffff
           , f 1 0x7fffffff := 0x7fffffff
           , f 0x80000000 1 := 0x80000000
           , f 1 0x80000000 := 0x80000000
           , f 0x7fffffff 2 := 0xfffffffe
           , f 2 0x7fffffff := 0xfffffffe
           , f 0x7fffffff 0 := 0
           , f 0 0x7fffffff := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x80000000 2 := 0
           , f 2 0x80000000 := 0
           , f 0x7fffffff 0x80000000 := 0x80000000
           , f 0x80000000 0x7fffffff := 0x80000000
           , f 0x7fffffff 0xffffffff := 0x80000001
           , f 0xffffffff 0x7fffffff := 0x80000001
           , f 0xffffffff 0xffffffff := 1 ]
#else
           , f 0x80000000 2 := 0x100000000
           , f 2 0x80000000 := 0x100000000
           , f 0x7fffffff 0x80000000 := 0x3fffffff80000000
           , f 0x80000000 0x7fffffff := 0x3fffffff80000000
           , f 0x7fffffff 0xffffffff := 0x7ffffffe80000001
           , f 0xffffffff 0x7fffffff := 0x7ffffffe80000001
           , f 0xffffffff 0xffffffff := 0xfffffffe00000001
           , f 0x7fffffffffffffff 1 := 0x7fffffffffffffff
           , f 1 0x7fffffffffffffff := 0x7fffffffffffffff
           , f 0x8000000000000000 1 := 0x8000000000000000
           , f 1 0x8000000000000000 := 0x8000000000000000
           , f 0x7fffffffffffffff 2 := 0xfffffffffffffffe
           , f 2 0x7fffffffffffffff := 0xfffffffffffffffe
           , f 0x8000000000000000 2 := 0
           , f 2 0x8000000000000000 := 0
           , f 0x7fffffffffffffff 0x8000000000000000 := 0x8000000000000000
           , f 0x8000000000000000 0x7fffffffffffffff := 0x8000000000000000
           , f 0x7fffffffffffffff 0xffffffffffffffff := 0x8000000000000001
           , f 0xffffffffffffffff 0x7fffffffffffffff := 0x8000000000000001
           , f 0xffffffffffffffff 0xffffffffffffffff := 1 ]
#endif

 , "mulr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1]

    jit_mulr_d j jit_F0 jit_F0 jit_F1
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f, _) <- jit_end j dyncall_DDD
    unlock
    expect [ f (-0.5) 0.5  := -0.25
           , f   0.25 0.75 := 0.1875 ]

 , "mulr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1]

    jit_mulr_f j jit_F0 jit_F0 jit_F1
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f, _) <- jit_end j dyncall_FFF
    unlock
    expect [ f (-0.5) 0.5 := -0.25
           , f 0.25 0.75 := 0.1875 ]

 , "negr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_negr j jit_R0 jit_R0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
    expect [ f 0 := 0
#if WORD_SIZE_IN_BITS == 32
           , f 1 := 0xffffffff
           , f 0xffffffff := 1
           , f 0x80000000 := 0x80000000
           , f 0x7fffffff := 0x80000001
           , f 0x80000001 := 0x7fffffff ]
#else
           , f 1 := 0xffffffffffffffff
           , f 0xffffffff := 0xffffffff00000001
           , f 0x80000000 := 0xffffffff80000000
           , f 0x7fffffff := 0xffffffff80000001
           , f 0x80000001 := 0xffffffff7fffffff
           , f 0xffffffffffffffff := 1
           , f 0x8000000000000000 := 0x8000000000000000
           , f 0x7fffffffffffffff := 0x8000000000000001 ]
#endif

 , "negr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

    jit_negr_d j jit_F0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f,_) <- jit_end j dyncall_DD
    unlock
    expect [ f 0.0 := -0.0
           , f 0.5 := -0.5
           , f (1.0 / 0.0) := -1.0 / 0.0
           , f (-1.25) := 1.25 ]

 , "negr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

    jit_negr_f j jit_F0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f,_) <- jit_end j dyncall_FF
    unlock
    expect [ f 0.0 := -0.0
           , f 0.5 := -0.5
           , f (1.0 / 0.0) := -1.0 / 0.0
           , f (-1.25) := 1.25 ]

 , "ori" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_ori j jit_R0 jit_R0 1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UU
    unlock
    expect $ [ f 0x7fffffff := 0x7fffffff
             , f 0x80000000 := 0x80000001 ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0x7fffffffffffffff := 0x7fffffffffffffff
             , f 0x8000000000000000 := 0x8000000000000001 ]
#endif

 , "orr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_orr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect $ [ f 0x7fffffff 1 := 0x7fffffff
             , f 1 0x7fffffff := 0x7fffffff
             , f 0x80000000 1 := 0x80000001
             , f 1 0x80000000 := 0x80000001
             , f 0x7fffffff 0x80000000 := 0xffffffff
             , f 0x80000000 0x7fffffff := 0xffffffff
             , f 0x7fffffff 0xffffffff := 0xffffffff
             , f 0xffffffff 0x7fffffff := 0xffffffff
             , f 0xffffffff 0xffffffff := 0xffffffff
             , f 0x7fffffff 0 := 0x7fffffff
             , f 0 0x7fffffff := 0x7fffffff ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0x7fffffffffffffff 1 := 0x7fffffffffffffff
             , f 1 0x7fffffffffffffff := 0x7fffffffffffffff
             , f 0x8000000000000000 1 := 0x8000000000000001
             , f 1 0x8000000000000000 := 0x8000000000000001
             , f 0x7fffffffffffffff 0x8000000000000000 := 0xffffffffffffffff
             , f 0x8000000000000000 0x7fffffffffffffff := 0xffffffffffffffff
             , f 0x7fffffffffffffff 0xffffffffffffffff := 0xffffffffffffffff
             , f 0xffffffffffffffff 0x7fffffffffffffff := 0xffffffffffffffff
             , f 0xffffffffffffffff 0xffffffffffffffff := 0xffffffffffffffff ]
#endif

 , "qdivr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 3 0 0

    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                    ,JitOperandGpr JitOperandAbiPointer jit_R1 0
                    ,JitOperandGpr jitOperandAbiWord jit_R2 0
                    ,JitOperandGpr jitOperandAbiWord jit_V0 0]

    jit_qdivr j jit_V1 jit_V2 jit_R2 jit_V0
    jit_str j jit_R0 jit_V1
    jit_str j jit_R1 jit_V2

    jit_leave_jit_abi j 3 0 align
    jit_ret j

    (f0, _) <- jit_end j dyncall_VPPII
    unlock

    let f x y = with (31337::Int) $ \u ->
                with (31337::Int) $ \v ->
                f0 u v x y >>
                liftM2 (,) (peek u) (peek v)

    expect [ f 10 3 := (3, 1)
           , f (-33) 9 := (-3, -6)
           , f (-41) (-7) := (5, -6)
           , f 65536 4096 := (16, 0) ]

 , "qdivr_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 3 0 0

    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                    ,JitOperandGpr JitOperandAbiPointer jit_R1 0
                    ,JitOperandGpr jitOperandAbiWord jit_R2 0
                    ,JitOperandGpr jitOperandAbiWord jit_V0 0]

    jit_qdivr_u j jit_V1 jit_V2 jit_R2 jit_V0
    jit_str j jit_R0 jit_V1
    jit_str j jit_R1 jit_V2
    jit_leave_jit_abi j 3 0 align
    jit_ret j

    (f0, _) <- jit_end j dyncall_VPPII
    unlock

    let f x y = with (31337::Int) $ \u ->
                with (31337::Int) $ \v ->
                f0 u v x y >>
                liftM2 (,) (peek u) (peek v)

    expect [ f (-1) (-2) := (1, 1)
           , f (-2) (-5) := (1, 3) ]

 , "qmulr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 3 0 0

    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                    ,JitOperandGpr JitOperandAbiPointer jit_R1 0
                    ,JitOperandGpr jitOperandAbiWord jit_R2 0
                    ,JitOperandGpr jitOperandAbiWord jit_V0 0]

    jit_qmulr j jit_V1 jit_V2 jit_R2 jit_V0
    jit_str j jit_R0 jit_V1
    jit_str j jit_R1 jit_V2
    jit_leave_jit_abi j 3 0 align
    jit_ret j

    (f0, _) <- jit_end j dyncall_VPPUU
    unlock

    let f x y = with (31337::Word) $ \u ->
                with (31337::Word) $ \v ->
                f0 u v x y >>
                liftM2 (,) (peek u) (peek v)

    expect [ f (complement 1) (complement 0) := (2, 0)
           , f             0  (complement 0) := (0, 0)
           , f (complement 0)             0  := (0, 0)
           , f             1  (complement 0) := (complement 0,complement 0)
#if WORD_SIZE_IN_BITS == 32
           , f 0x7ffff 0x7ffff := (0xfff00001, 0x3f)
           , f 0x80000000 (complement 1) := (0, 1)
           , f 0x80000000             2  := (0, complement 0)
           , f 0x80000001             3  := (0x80000003, complement 1)
           , f 0x80000001 (complement 2) := (0x7ffffffd, 1) ]
#else
           , f 0x7ffffffff 0x7ffffffff := (0xfffffff000000001, 0x3f)
           , f 0x8000000000000000 (complement 1) := (0, 1)
           , f 0x8000000000000000             2  := (0, complement 0)
           , f 0x8000000000000001             3  := (0x8000000000000003, complement 1)
           , f 0x8000000000000001 (complement 2) := (0x7ffffffffffffffd, 1) ]
#endif

 , "qmulr_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 3 0 0

    jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                    ,JitOperandGpr JitOperandAbiPointer jit_R1 0
                    ,JitOperandGpr jitOperandAbiWord jit_R2 0
                    ,JitOperandGpr jitOperandAbiWord jit_V0 0]

    jit_qmulr_u j jit_V1 jit_V2 jit_R2 jit_V0
    jit_str j jit_R0 jit_V1
    jit_str j jit_R1 jit_V2
    jit_leave_jit_abi j 3 0 align
    jit_ret j

    (f0, _) <- jit_end j dyncall_VPPUU
    unlock

    let f x y = with (31337::Word) $ \u ->
                with (31337::Word) $ \v ->
                f0 u v x y >>
                liftM2 (,) (peek u) (peek v)

#if WORD_SIZE_IN_BITS == 32
    expect [ f 0xffffff 0xffffff := (0xfe000001, 0xffff) ]
#else
    expect [ f 0xffffffffff 0xffffffffff := (0xfffffe0000000001, 0xffff) ]
#endif

 , "remr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_remr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0x7fffffff 1 := 0
           , f 1 0x7fffffff := 1
           , f 0x80000000 1 := 0
           , f 1 0x80000000 := 1
           , f 0x7fffffff 2 := 1
           , f 2 0x7fffffff := 2
           , f 0x80000000 2 := 0
           , f 2 0x80000000 := 2
           , f 0x7fffffff 0x80000000 := 0x7fffffff
           , f 0 0x7fffffff := 0
           , f 0xffffffff 0xffffffff := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x80000000 0x7fffffff := 0xffffffff
           , f 0x7fffffff 0xffffffff := 0
           , f 0xffffffff 0x7fffffff := 0xffffffff ]
#else
           , f 0x80000000 0x7fffffff := 1
           , f 0x7fffffff 0xffffffff := 0x7fffffff
           , f 0xffffffff 0x7fffffff := 1
           , f 0x7fffffffffffffff 1 := 0
           , f 1 0x7fffffffffffffff := 1
           , f 0x8000000000000000 1 := 0
           , f 1 0x8000000000000000 := 1
           , f 0x7fffffffffffffff 2 := 1
           , f 2 0x7fffffffffffffff := 2
           , f 0x8000000000000000 2 := 0
           , f 2 0x8000000000000000 := 2
           , f 0x7fffffffffffffff 0x8000000000000000 := 0x7fffffffffffffff
           , f 0x8000000000000000 0x7fffffffffffffff := 0xffffffffffffffff
           , f 0x7fffffffffffffff 0xffffffffffffffff := 0
           , f 0xffffffffffffffff 0x7fffffffffffffff := 0xffffffffffffffff
           , f 0xffffffffffffffff 0xffffffffffffffff := 0 ]
#endif

 , "remr_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_remr_u j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect $ [ f 0x7fffffff 1 := 0
             , f 1 0x7fffffff := 1
             , f 0x80000000 1 := 0
             , f 1 0x80000000 := 1
             , f 0x7fffffff 2 := 1
             , f 2 0x7fffffff := 2
             , f 0x80000000 2 := 0
             , f 2 0x80000000 := 2
             , f 0x7fffffff 0x80000000 := 0x7fffffff
             , f 0x80000000 0x7fffffff := 1
             , f 0 0x7fffffff := 0
             , f 0x7fffffff 0xffffffff := 0x7fffffff
             , f 0xffffffff 0x7fffffff := 1
             , f 0xffffffff 0xffffffff := 0 ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0x7fffffffffffffff 1 := 0
             , f 1 0x7fffffffffffffff := 1
             , f 0x8000000000000000 1 := 0
             , f 1 0x8000000000000000 := 1
             , f 0x7fffffffffffffff 2 := 1
             , f 2 0x7fffffffffffffff := 2
             , f 0x8000000000000000 2 := 0
             , f 2 0x8000000000000000 := 2
             , f 0x7fffffffffffffff 0x8000000000000000 := 0x7fffffffffffffff
             , f 0x8000000000000000 0x7fffffffffffffff := 1
             , f 0x7fffffffffffffff 0xffffffffffffffff := 0x7fffffffffffffff
             , f 0xffffffffffffffff 0x7fffffffffffffff := 1
             , f 0xffffffffffffffff 0xffffffffffffffff := 0 ]
#endif

 , "rshi" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_rshi j jit_R0 jit_R0 31
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
#if WORD_SIZE_IN_BITS == 32
    expect [ f 0x80000000 := 0xffffffff ]
#else
    expect [ f 0x80000000 := 1
           , f 0x8000000000000000 := 0xffffffff00000000 ]
#endif

 , "rshi_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_rshi_u j jit_R0 jit_R0 31
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UU
    unlock
#if WORD_SIZE_IN_BITS == 32
    expect [ f 0x80000000 := 1 ]
#else
    expect [ f 0x80000000 := 1
           , f 0x8000000000000000 := 0x100000000 ]
#endif

 , "rshr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_rshr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0xfe 1 := 0x7f
           , f 0x1fffc 2 := 0x7fff
           , f 0x40000000 30 := 1
           , f 0x20000000 29 := 1
           , f 0x10000000 28 := 1
           , f 0x810000 16 := 0x81
           , f 0x20000 17 := 1
           , f 0x40000 18 := 1
           , f 0x7f8000 15 := 0xff
           , f 0x1000000 24 := 1
           , f 0x7fffffff 0 := 0x7fffffff
#if WORD_SIZE_IN_BITS == 32
           , f 0xfffffff8 3 := 0xffffffff
           , f 0x80000000 31 := 0xffffffff
           , f 0xffffff00 8 := 0xffffffff ]
#else
           , f 0x3fffffff8 3 := 0x7fffffff
           , f 0xffffffc080000000 31 := 0xffffffffffffff81
           , f 0xffffff00 8 := 0xffffff
           , f 0xfe00000000 33 := 0x7f
           , f 0x1ffffc00000000 34 := 0x7ffff
           , f 0xfffffff800000000 29 := 0xffffffffffffffc0
           , f 0x8000000000000000 63 := 0xffffffffffffffff
           , f 0x4000000000000000 62 := 1
           , f 0x2000000000000000 61 := 1
           , f 0x1000000000000000 60 := 1
           , f 0x81000000000000 48 := 0x81
           , f 0x2000000000000 49 := 1
           , f 0x10000000000 40 := 1
           , f 0x7f800000000000 47 := 0xff
           , f 0x100000000000000 56 := 1
           , f 0xffffff0000000000 40 := 0xffffffffffffffff
           , f 0xfffffffe00000000 33 := 0xffffffffffffffff
           , f 0x8000000000000001 63 := 0xffffffffffffffff
           , f 0x1000000000000 48 := 1
           , f 0xffff800000000000 47 := 0xffffffffffffffff ]
#endif

 , "rshr_u" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_rshr_u j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0xfe 1 := 0x7f
           , f 0x1fffc 2 := 0x7fff
           , f 0x80000000 31 := 1
           , f 0x40000000 30 := 1
           , f 0x20000000 29 := 1
           , f 0x10000000 28 := 1
           , f 0x810000 16 := 0x81
           , f 0x20000 17 := 1
           , f 0x40000 18 := 1
           , f 0x7f8000 15 := 0xff
           , f 0x1000000 24 := 1
           , f 0xffffff00 8 := 0xffffff
           , f 0x7fffffff 0 := 0x7fffffff
#if WORD_SIZE_IN_BITS == 32
           , f 0xfffffff8 3 := 0x1fffffff ]
#else
           , f 0x3fffffff8 3 := 0x7fffffff
           , f 0xffffffc080000000 31 := 0x1ffffff81
           , f 0xfe00000000 33 := 0x7f
           , f 0x1ffffc00000000 34 := 0x7ffff
           , f 0xfffffff800000000 29 := 0x7ffffffc0
           , f 0x8000000000000000 63 := 1
           , f 0x4000000000000000 62 := 1
           , f 0x2000000000000000 61 := 1
           , f 0x1000000000000000 60 := 1
           , f 0x81000000000000 48 := 0x81
           , f 0x2000000000000 49 := 1
           , f 0x10000000000 40 := 1
           , f 0x7f800000000000 47 := 0xff
           , f 0x100000000000000 56 := 1
           , f 0xffffff0000000000 40 := 0xffffff
           , f 0xfffffffe00000000 33 := 0x7fffffff
           , f 0x8000000000000001 63 := 1
           , f 0x1000000000000 48 := 1
           , f 0xffff800000000000 47 := 0x1ffff ]
#endif

 , "sqrtr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

    jit_sqrtr_d j jit_F0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f,_) <- jit_end j dyncall_DD
    unlock

    expect [ f   0.0 := 0.0
           , f   4.0 := 2.0
           , f (-4.0) :~ isNaN ]

 , "sqrtr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0]

    jit_sqrtr_f j jit_F0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f,_) <- jit_end j dyncall_FF
    unlock
    expect [ f 0.0 := 0.0
           , f 4.0 := 2.0
           , f (-4.0) :~ isNaN ]

 , "sti_c" $$ \j unlock -> do
    withArray [ 0x12, 0x00, 0x34 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiInt8 jit_R1 0]

        jit_sti_c j (advancePtr dat 1) jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VI8
        unlock
        expect [ f   0  >> peekArray 3 dat := [0x12,0x00,0x34]
               , f (-1) >> peekArray 3 dat := [0x12,0xff,0x34] ]

 , "sti_d" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

        jit_sti_d j (advancePtr dat 1) jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VD
        unlock
        expect [ f   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "sti_f" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0]

        jit_sti_f j (advancePtr dat 1) jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VF
        unlock
        expect [ f   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "sti_i" $$ \j unlock -> do
    withArray [ 0x12121212, 0x00000000, 0x34343434 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiInt32 jit_R1 0]

        jit_sti_i j (advancePtr dat 1) jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VI32
        unlock
        expect [ f   0  >> peekArray 3 dat := [0x12121212,0x00000000,0x34343434]
               , f (-1) >> peekArray 3 dat := [0x12121212,0xffffffff,0x34343434] ]

#if WORD_SIZE_IN_BITS > 32
 , "sti_l" $$ \j unlock -> do
    withArray [ 0x1212121212121212, 0, 0x3434343434343434 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiInt64 jit_R1 0]

        jit_sti_l j (advancePtr dat 1) jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VI64
        unlock
        expect [ f   0  >> peekArray 3 dat := [0x1212121212121212,0x0000000000000000,0x3434343434343434]
               , f (-1) >> peekArray 3 dat := [0x1212121212121212,0xffffffffffffffff,0x3434343434343434] ]
#endif

 , "sti_s" $$ \j unlock -> do
    withArray [ 0x1212, 0x0000, 0x3434 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiInt16 jit_R1 0]

        jit_sti_s j (advancePtr dat 1) jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VI16
        unlock
        expect [ f   0  >> peekArray 3 dat := [0x1212,0x0000,0x3434]
               , f (-1) >> peekArray 3 dat := [0x1212,0xffff,0x3434] ]

 , "str_c" $$ \j unlock -> do
    withArray [ 0x12, 0x00, 0x34 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr JitOperandAbiInt8 jit_R1 0]

        jit_str_c j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPI8
        unlock
        expect [ f (advancePtr dat 1)   0  >> peekArray 3 dat := [0x12,0x00,0x34]
               , f (advancePtr dat 1) (-1) >> peekArray 3 dat := [0x12,0xff,0x34] ]

 , "str_d" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandFpr JitOperandAbiDouble jit_F0]

        jit_str_d j jit_R0 jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPD
        unlock
        expect [ f (advancePtr dat 1)   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f (advancePtr dat 1) 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "str_f" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandFpr JitOperandAbiFloat jit_F0]

        jit_str_f j jit_R0 jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPF
        unlock
        expect [ f (advancePtr dat 1)   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f (advancePtr dat 1) 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "str_i" $$ \j unlock -> do
    withArray [ 0x12121212, 0x00000000, 0x34343434 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr JitOperandAbiInt32 jit_R1 0]

        jit_str_i j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPI32
        unlock
        expect [ f (advancePtr dat 1)   0  >> peekArray 3 dat := [0x12121212,0x00000000,0x34343434]
               , f (advancePtr dat 1) (-1) >> peekArray 3 dat := [0x12121212,0xffffffff,0x34343434] ]

#if WORD_SIZE_IN_BITS > 32
 , "str_l" $$ \j unlock -> do
    withArray [ 0x1212121212121212, 0, 0x3434343434343434 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr JitOperandAbiInt64 jit_R1 0]

        jit_str_l j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPI64
        unlock
        expect [ f (advancePtr dat 1)   0  >> peekArray 3 dat := [0x1212121212121212,0x0000000000000000,0x3434343434343434]
               , f (advancePtr dat 1) (-1) >> peekArray 3 dat := [0x1212121212121212,0xffffffffffffffff,0x3434343434343434]]
#endif

 , "str_s" $$ \j unlock -> do
    withArray [ 0x1212, 0x0000, 0x3434 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr JitOperandAbiInt16 jit_R1 0]

        jit_str_s j jit_R0 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPI16
        unlock
        expect [ f (advancePtr dat 1)   0  >> peekArray 3 dat := [0x1212,0x0000,0x3434]
               , f (advancePtr dat 1) (-1) >> peekArray 3 dat := [0x1212,0xffff,0x3434] ]

 , "stxi_c" $$ \j unlock -> do
    withArray [ 0x12, 0x00, 0x34 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt8 jit_R1 0]

        jit_stxi_c j (fromIntegral $ ptrToIntPtr dat) jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VII8
        unlock
        expect [ f 1   0  >> peekArray 3 dat := [0x12,0x00,0x34]
               , f 1 (-1) >> peekArray 3 dat := [0x12,0xff,0x34] ]

 , "stxi_d" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandFpr JitOperandAbiDouble jit_F0]

        jit_stxi_d j (fromIntegral $ ptrToIntPtr dat) jit_R2 jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VID
        unlock
        expect [ f 8   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f 8 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "stxi_f" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandFpr JitOperandAbiFloat jit_F0]

        jit_stxi_f j (fromIntegral $ ptrToIntPtr dat) jit_R2 jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VIF
        unlock
        expect [ f 4   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f 4 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "stxi_i" $$ \j unlock -> do
    withArray [ 0x12121212, 0x00000000, 0x34343434 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt32 jit_R1 0]

        jit_stxi_i j (fromIntegral $ ptrToIntPtr dat) jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VII32
        unlock
        expect [ f 4   0  >> peekArray 3 dat := [0x12121212,0x00000000,0x34343434]
               , f 4 (-1) >> peekArray 3 dat := [0x12121212,0xffffffff,0x34343434] ]

#if WORD_SIZE_IN_BITS > 32
 , "stxi_l" $$ \j unlock -> do
    withArray [ 0x1212121212121212, 0, 0x3434343434343434 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt64 jit_R1 0]

        jit_stxi_l j (fromIntegral $ ptrToIntPtr dat) jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VII64
        unlock
        expect [ f 8   0  >> peekArray 3 dat := [0x1212121212121212,0x0000000000000000,0x3434343434343434]
               , f 8 (-1) >> peekArray 3 dat := [0x1212121212121212,0xffffffffffffffff,0x3434343434343434] ]
#endif

 , "stxi_s" $$ \j unlock -> do
    withArray [ 0x1212, 0x0000, 0x3434 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt16 jit_R1 0]

        jit_stxi_s j (fromIntegral $ ptrToIntPtr dat) jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VII16
        unlock
        expect [ f 2   0  >> peekArray 3 dat := [0x1212,0x0000,0x3434]
               , f 2 (-1) >> peekArray 3 dat := [0x1212,0xffff,0x3434] ]

 , "stxr_c" $$ \j unlock -> do
    withArray [ 0x12, 0x00, 0x34 :: Word8 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt8 jit_R1 0]

        jit_stxr_c j jit_R0 jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPII8
        unlock
        expect [ f dat 1   0  >> peekArray 3 dat := [0x12,0x00,0x34]
               , f dat 1 (-1) >> peekArray 3 dat := [0x12,0xff,0x34] ]

 , "stxr_d" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Double ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandFpr JitOperandAbiDouble jit_F0 ]

        jit_stxr_d j jit_R0 jit_R2 jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPID
        unlock
        expect [ f dat 8   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f dat 8 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "stxr_f" $$ \j unlock -> do
    withArray [ -1.0, 0.0, 0.5 :: Float ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandFpr JitOperandAbiFloat jit_F0 ]

        jit_stxr_f j jit_R0 jit_R2 jit_F0
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPIF
        unlock
        expect [ f dat 4   0  >> peekArray 3 dat := [-1.0, 0.0,0.5]
               , f dat 4 42.5 >> peekArray 3 dat := [-1.0,42.5,0.5] ]

 , "stxr_i" $$ \j unlock -> do
    withArray [ 0x12121212, 0x00000000, 0x34343434 :: Word32 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt32 jit_R1 0]

        jit_stxr_i j jit_R0 jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPII32
        unlock
        expect [ f dat 4   0  >> peekArray 3 dat := [0x12121212,0x00000000,0x34343434]
               , f dat 4 (-1) >> peekArray 3 dat := [0x12121212,0xffffffff,0x34343434] ]

#if WORD_SIZE_IN_BITS > 32
 , "stxr_l" $$ \j unlock -> do
    withArray [ 0x1212121212121212, 0, 0x3434343434343434 :: Word64 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt64 jit_R1 0]

        jit_stxr_l j jit_R0 jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPII64
        unlock
        expect [ f dat 8   0  >> peekArray 3 dat := [0x1212121212121212,0x0000000000000000,0x3434343434343434]
               , f dat 8 (-1) >> peekArray 3 dat := [0x1212121212121212,0xffffffffffffffff,0x3434343434343434] ]
#endif

 , "stxr_s" $$ \j unlock -> do
    withArray [ 0x1212, 0, 0x3434 :: Word16 ] $ \dat -> do
        align <- jit_enter_jit_abi j 0 0 0
        jit_load_args j [JitOperandGpr JitOperandAbiPointer jit_R0 0
                        ,JitOperandGpr jitOperandAbiWord jit_R2 0
                        ,JitOperandGpr JitOperandAbiInt16 jit_R1 0]

        jit_stxr_s j jit_R0 jit_R2 jit_R1
        jit_leave_jit_abi j 0 0 align
        jit_ret j

        (f,_) <- jit_end j dyncall_VPII16
        unlock
        expect [ f dat 2   0  >> peekArray 3 dat := [0x1212,0x0000,0x3434]
               , f dat 2 (-1) >> peekArray 3 dat := [0x1212,0xffff,0x3434] ]

 , "subr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_subr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_III
    unlock
    expect [ f 42 69 := -27 ]

 , "subr_d" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0
                    ,JitOperandFpr JitOperandAbiDouble jit_F1 ]

    jit_subr_d j jit_F0 jit_F0 jit_F1
    jit_leave_jit_abi j 0 0 align
    jit_retr_d j jit_F0

    (f, _) <- jit_end j dyncall_DDD
    unlock
    expect [ f 42.0 69.0 := -27.0
           , f 42.0 69.5 := -27.5 ]

 , "subr_f" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0
                    ,JitOperandFpr JitOperandAbiFloat jit_F1 ]

    jit_subr_f j jit_F0 jit_F0 jit_F1
    jit_leave_jit_abi j 0 0 align
    jit_retr_f j jit_F0

    (f, _) <- jit_end j dyncall_FFF
    unlock
    expect [ f 42.0 69.0 := -27.0
           , f 42.0 69.5 := -27.5 ]

 , "subxi" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0]

    jit_movi j jit_R2 0
    jit_subcr j jit_R0 jit_R0 jit_R1
    jit_subxi j jit_R2 jit_R2 0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R2

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect [ f 0 0 := 0
#if WORD_SIZE_IN_BITS == 32
           , f 0x7fffffff 0xffffffff := 0xffffffff              -- carry
           , f 0x80000000 1 := 0                                -- overflow
           , f 0x7fffffff 0x80000000 := 0xffffffff              -- carry
           , f 0x80000000 0x7fffffff := 0                       -- overflow
           , f 1 0x80000000 := 0xffffffff ]                     -- carry+overflow
#else
           , f 0x7fffffff 0xffffffff := complement 0                    -- carry
           , f 0x80000000 1 := 0                                        -- nothing
           , f 0x7fffffff 0x80000000 := complement 0                    -- carry
           , f 0x80000000 0x7fffffff := 0                               -- nothing
           , f 1 0x80000000 := complement 0                             -- carry
           , f 0x7fffffffffffffff 0xffffffffffffffff := complement 0    -- carry
           , f 0x8000000000000000 1 := 0                                -- overflow
           , f 0x7fffffffffffffff 0x8000000000000000 := complement 0    -- carry
           , f 0x8000000000000000 0x7fffffffffffffff := 0               -- overflow
           , f 1 0x8000000000000000 := complement 0 ]                   -- carry+overflow
#endif

 , "truncr_d_i" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

    jit_truncr_d_i j jit_R0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_I32D
    unlock
    expect [ f   0.0  := 0
           , f (-0.0) := 0
           , f   0.5  := 0
           , f (-0.5) := 0
           , f   1.5  := 1
           , f (-1.5) := -1
           , f   2.5  := 2
           , f (-2.5) := -2 ]

#if WORD_SIZE_IN_BITS > 32
 , "truncr_d_l" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiDouble jit_F0]

    jit_truncr_d_l j jit_R0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_I64D
    unlock
    expect [ f   0.0  := 0
           , f (-0.0) := 0
           , f   0.5  := 0
           , f (-0.5) := 0
           , f   1.5  := 1
           , f (-1.5) := -1
           , f   2.5  := 2
           , f (-2.5) := -2 ]
#endif

 , "truncr_f_i" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0]

    jit_truncr_f_i j jit_R0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_I32F
    unlock
    expect [ f   0.0  := 0
           , f (-0.0) := 0
           , f   0.5  := 0
           , f (-0.5) := 0
           , f   1.5  := 1
           , f (-1.5) := -1
           , f   2.5  := 2
           , f (-2.5) := -2 ]

#if WORD_SIZE_IN_BITS > 32
 , "truncr_f_l" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandFpr JitOperandAbiFloat jit_F0]

    jit_truncr_f_l j jit_R0 jit_F0
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f,_) <- jit_end j dyncall_I64F
    unlock
    expect [ f   0.0  := 0
           , f (-0.0) := 0
           , f   0.5  := 0
           , f (-0.5) := 0
           , f   1.5  := 1
           , f (-1.5) := -1
           , f   2.5  := 2
           , f (-2.5) := -2 ]
#endif

 , "xori" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0]

    jit_xori j jit_R0 jit_R0 1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UU
    unlock
    expect $ [ f 0x7fffffff := 0x7ffffffe
             , f 0x80000000 := 0x80000001 ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0x7fffffffffffffff := 0x7ffffffffffffffe
             , f 0x8000000000000000 := 0x8000000000000001 ]
#endif

 , "xorr" $$ \j unlock -> do
    align <- jit_enter_jit_abi j 0 0 0
    jit_load_args j [JitOperandGpr jitOperandAbiWord jit_R0 0
                    ,JitOperandGpr jitOperandAbiWord jit_R1 0 ]

    jit_xorr j jit_R0 jit_R0 jit_R1
    jit_leave_jit_abi j 0 0 align
    jit_retr j jit_R0

    (f, _) <- jit_end j dyncall_UUU
    unlock
    expect $ [ f 0x7fffffff 1 := 0x7ffffffe
             , f 1 0x7fffffff := 0x7ffffffe
             , f 0x80000000 1 := 0x80000001
             , f 1 0x80000000 := 0x80000001
             , f 0x7fffffff 0x80000000 := 0xffffffff
             , f 0x80000000 0x7fffffff := 0xffffffff
             , f 0x7fffffff 0xffffffff := 0x80000000
             , f 0xffffffff 0x7fffffff := 0x80000000
             , f 0xffffffff 0xffffffff := 0
             , f 0x7fffffff 0 := 0x7fffffff
             , f 0 0x7fffffff := 0x7fffffff ]
#if WORD_SIZE_IN_BITS > 32
          ++ [ f 0x7fffffffffffffff 1 := 0x7ffffffffffffffe
             , f 1 0x7fffffffffffffff := 0x7ffffffffffffffe
             , f 0x8000000000000000 1 := 0x8000000000000001
             , f 1 0x8000000000000000 := 0x8000000000000001
             , f 0x7fffffffffffffff 0x8000000000000000 := 0xffffffffffffffff
             , f 0x8000000000000000 0x7fffffffffffffff := 0xffffffffffffffff
             , f 0x7fffffffffffffff 0xffffffffffffffff := 0x8000000000000000
             , f 0xffffffffffffffff 0x7fffffffffffffff := 0x8000000000000000
             , f 0xffffffffffffffff 0xffffffffffffffff := 0 ]
#endif
 ]

