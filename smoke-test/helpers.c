#include <stdint.h>

int tail_call_test(void)
{
    return 42;
}

int call_10_test(int32_t a, int32_t b, int32_t c, int32_t d, int32_t e,
                 int32_t f, int32_t g, int32_t h, int32_t i, int32_t j)
{
  return 1*a + 2*b + 3*c + 4*d + 5*e + 6*f + 7*g + 8*h + 9*i + 10*j ;
}
