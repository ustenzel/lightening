module Development.Lightening.Gen_Instructions
    ( parse_header, generate_instructions, generate_wrappers ) where

import Data.List

{-  lightening.h has a macro FOR_EACH_INSTRUCTION, which iterates over
 -  the instruction set.  It would be great if we could expand it in a
 -  way that gives us the foreign imports.  That doesn't seem possible,
 -  though:  the C preprocessor will not generate the necessary
 -  newlines, and it requires token concatenation, which appears
 -  unreliable in cpphs.
 -
 -  So, instead, we parse the instruction list from that macro
 -  definition, and generate a Haskell source file.  This is run from
 -  Setup.hs during the build phase.  -}

generate_instructions :: [(String,String)] -> String
generate_instructions =
    (++) "module Development.Lightening.Instructions where\n\
         \import Foreign.C.Types\n\
         \import Foreign.Ptr\n\
         \import Development.Lightening.Jit\n\
         \import Development.Lightening.Operand\n\n"
    . concatMap (uncurry gen_foreign_import)

gen_foreign_import :: String -> String -> String
gen_foreign_import [         ] name = error $ show name ++ " has an empty type"
gen_foreign_import (rtp:types) name = unlines $
    case rtp of
        '_' -> [ "foreign import ccall unsafe \"lightening.h  jit_" ++ name ++ "\""
               , "  jit_" ++ name ++ " :: JitState -> " ++ expand_types ++ " IO ()"
               , ""
               , name ++ " :: " ++ expand_types ++ " Jit ()"
               , name ++ " " ++ args ++ " = Jit $ \\k j -> jit_" ++ name ++ " j " ++ args ++ " >>= \\a -> k a j" ]
        'R' -> [ "foreign import ccall unsafe \"lightening.h wrap_jit_" ++ name ++ "\""
               , "  jit_" ++ name ++ " :: JitState -> JitReloc -> " ++ expand_types ++ " IO ()"
               , ""
               , name ++ " :: " ++ expand_types ++ " Jit JitReloc"
               , name ++ " " ++ args ++ " = Jit $ \\k j -> withReloc $ \\r ->"
               , "  jit_" ++ name ++ " j r " ++ args ++ " >> k r j" ]
        _   -> strange name rtp
  where
    expand_types = concatMap tp types

    tp 'G' = "JitGprT -> "
    tp 'F' = "JitFprT -> "
    tp 'o' = "JitOffT -> "
    tp 'p' = "Ptr a -> "
    tp 'i' = "JitImmT -> "
    tp 'u' = "JitUimmT -> "
    tp 'f' = "Float -> "
    tp 'd' = "Double -> "
    tp '_' = ""
    tp  x  = strange name x

    args = unwords $ zipWith (\i _ -> "x" ++ show i) [0::Int ..] $ filter (/='_') types


strange :: String -> Char -> a
strange name x = error $ show name ++ " uses the strange type " ++ show x

generate_wrappers :: [(String,String)] -> String
generate_wrappers =
    (++) "#include \"lightening.h\"\n\n" .
    (++) "void wrap_jit_patch_here(jit_state_t *_jit, jit_reloc_t *reloc)\n" .
    (++) "{ jit_patch_there (_jit, *reloc, jit_address (_jit)); }\n\n" .
    (++) "void wrap_jit_patch_there(jit_state_t *_jit, jit_reloc_t *reloc, jit_pointer_t addr)\n" .
    (++) "{ jit_patch_there (_jit, *reloc, addr); }\n\n" .
    concatMap go
  where
    go ('R':ts,nm) = gen_wrapper nm (zip [1..] ts)
    go      _      = ""

gen_wrapper :: String -> [(Int,Char)] -> String
gen_wrapper nm ts =
    "void wrap_jit_" ++ nm ++ "( jit_state_t *j, jit_reloc_t *r" ++ concatMap (uncurry arg) ts ++ " )\n" ++
    "{ *r = jit_" ++ nm ++ "( j" ++ concatMap (uncurry par) ts ++ " ) ; }\n\n"
  where
    arg n 'G' = ", jit_gpr_t x" ++ show n
    arg n 'F' = ", jit_fpr_t x" ++ show n
    arg n 'o' = ", jit_off_t x" ++ show n
    arg n 'p' = ", jit_pointer_t x" ++ show n
    arg n 'i' = ", jit_imm_t x" ++ show n
    arg n 'u' = ", jit_uimm_t x" ++ show n
    arg n 'f' = ", jit_float32_t x" ++ show n
    arg n 'd' = ", jit_float64_t x" ++ show n
    arg _ '_' = ""
    arg _  x  = strange nm x

    par n c | c `elem` "GFopiufd" = ", x" ++ show n
            | c == '_'            = ""
            | otherwise           = strange nm c


parse_header :: Bool -> String -> [(String,String)]
parse_header is64 = concatMap (parse1 . trim) . filter (not . is_gap) .
                    takeWhile is_continued . drop 1 . dropWhile (not . is_def) . lines
  where
    is_def = isPrefixOf "#define FOR_EACH_INSTRUCTION"
    is_continued s = not (null s) && last s == '\\'
    is_gap = all (`elem` " \t\\")

    trim = reverse . dropWhile (`elem` " \t\\") . reverse . dropWhile (`elem` " \t")

    parse1 s
        | "WHEN_64(" `isPrefixOf` s && last s == ')'
            = if is64 then parse1 $ init $ drop 8 s else []

        | "M(" `isPrefixOf` s && last s == ')'
            = case break (==',') (init (drop 2 s)) of
                (l,r) -> [(l, trim (drop 1 r))]

        | otherwise
            = error $ "this is unexpected: " ++ show s
