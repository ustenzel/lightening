module Development.Lightening.Jit where

import Development.Lightening.Operand

newtype Jit a = Jit { runJit :: forall r . (a -> JitState -> IO r) -> JitState -> IO r }

instance Functor Jit where
    fmap f j = Jit $ \k -> runJit j (k . f)

instance Applicative Jit where
    pure a = Jit $ \k -> k a
    jf <*> ja = Jit $ \k -> runJit jf (\f -> runJit ja (k . f))

instance Monad Jit where
    return = pure
    j >>= m = Jit $ \k -> runJit j (\a -> runJit (m a) k)

io :: IO a -> Jit a
io m = Jit $ \k s -> m >>= \a -> k a s

managed :: (forall b . (a -> IO b) -> IO b) -> Jit a
managed w = Jit $ \k s -> w (\a -> k a s)

ask :: Jit JitState
ask = Jit $ \k s -> k s s

