{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Development.Lightening.Operand where

#include "lightening.h"
#include "MachDeps.h"

import Data.Word
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable
import Text.Printf

data JitStateRep
type JitState = Ptr JitStateRep

type    JitImmT  = CIntPtr
type    JitUimmT = CUIntPtr
type    JitOffT  = CPtrdiff
newtype JitGprT  = JitGprT { gpr_regno :: Word8 } deriving Show
newtype JitFprT  = JitFprT { fpr_regno :: Word8 } deriving Show

data JitOperandAbi
    = JitOperandAbiUint8
    | JitOperandAbiInt8
    | JitOperandAbiUint16
    | JitOperandAbiInt16
    | JitOperandAbiUint32
    | JitOperandAbiInt32
    | JitOperandAbiUint64
    | JitOperandAbiInt64
    | JitOperandAbiPointer
    | JitOperandAbiFloat
    | JitOperandAbiDouble
  deriving ( Show, Enum )

jitOperandAbiWord :: JitOperandAbi
#if WORD_SIZE_IN_BITS == 64
jitOperandAbiWord = JitOperandAbiInt64
#elif WORD_SIZE_IN_BITS == 32
jitOperandAbiWord = JitOperandAbiInt32
#else
#error expected WORD_SIZE_IN_BITS to be 32 or 64
#endif

data JitOperand
    = JitOperandImm JitOperandAbi CIntPtr
    | JitOperandGpr JitOperandAbi JitGprT CPtrdiff
    | JitOperandFpr JitOperandAbi JitFprT
    | JitOperandMem JitOperandAbi JitGprT CPtrdiff CPtrdiff
  deriving Show

instance Storable JitOperand where
    sizeOf _ = #{ size jit_operand_t }
    alignment _ = #{ alignment jit_operand_t }

    poke p (JitOperandImm abi imm) = do
        #{ poke jit_operand_t, abi } p (fromEnum abi)
        #{ poke jit_operand_t, kind } p (#{ const JIT_OPERAND_KIND_IMM } :: #{ type enum jit_operand_kind })
        #{ poke jit_operand_t, loc.imm } p imm

    poke p (JitOperandGpr abi gpr add) = do
        #{ poke jit_operand_t, abi } p (fromEnum abi)
        #{ poke jit_operand_t, kind } p (#{ const JIT_OPERAND_KIND_GPR } :: #{ type enum jit_operand_kind })
        #{ poke jit_operand_t, loc.gpr.gpr } p (gpr_regno gpr)
        #{ poke jit_operand_t, loc.gpr.addend } p add

    poke p (JitOperandFpr abi fpr) = do
        #{ poke jit_operand_t, abi } p (fromEnum abi)
        #{ poke jit_operand_t, kind } p (#{ const JIT_OPERAND_KIND_FPR } :: #{ type enum jit_operand_kind })
        #{ poke jit_operand_t, loc.fpr } p (fpr_regno fpr)

    poke p (JitOperandMem abi gpr off add ) = do
        #{ poke jit_operand_t, abi } p (fromEnum abi)
        #{ poke jit_operand_t, kind } p (#{ const JIT_OPERAND_KIND_MEM } :: #{ type enum jit_operand_kind })
        #{ poke jit_operand_t, loc.mem.base } p (gpr_regno gpr)
        #{ poke jit_operand_t, loc.mem.offset } p off
        #{ poke jit_operand_t, loc.mem.addend } p add

    peek = undefined -- XXX


-- I think this gets returned as an uint64_t
-- typedef struct jit_reloc
-- {
--   uint8_t kind;
--   uint8_t inst_start_offset;
--   uint8_t pc_base_offset;
--   uint8_t rsh;
--   uint32_t offset;
-- } jit_reloc_t;

data JitRelocRep = JitRelocRep
type JitReloc = Ptr JitRelocRep

withReloc :: (JitReloc -> IO r) -> IO r
withReloc = allocaBytes #size jit_reloc_t

show_reloc :: JitReloc -> IO String
show_reloc p = do
    printf (concat [ "struct jit_reloc {"
                   , " .kind = %d,"
                   , " .inst_start_offset = %d,"
                   , " .pc_base_offset = %d,"
                   , " .rsh = %d,"
                   , " .offset = %d }" ])
        <$> (#{peek jit_reloc_t, kind}              p :: IO Word8)
        <*> (#{peek jit_reloc_t, inst_start_offset} p :: IO Word8)
        <*> (#{peek jit_reloc_t, pc_base_offset}    p :: IO Word8)
        <*> (#{peek jit_reloc_t, rsh}               p :: IO Word8)
        <*> (#{peek jit_reloc_t, offset}            p :: IO Word32)

