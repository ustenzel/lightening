module Development.Lightening.Arena
    ( newArena, unlockArena, freeArena )
    where

import Foreign.C.Types
import Foreign.Ptr
import System.Posix.Types

#include <sys/mman.h>

foreign import ccall unsafe "mmap"
    mmap :: Ptr a -> CSize -> CInt -> CInt -> CInt -> COff -> IO (Ptr a)

foreign import ccall unsafe "mprotect"
    mprotect :: Ptr a -> CSize -> CInt -> IO CInt

foreign import ccall unsafe "munmap"
    munmap :: Ptr a -> CSize -> IO ()

-- XXX  error handling?!
newArena :: CSize -> IO (Maybe (Ptr a))
newArena arena_size =
    Just <$> mmap nullPtr arena_size (#const PROT_READ | PROT_WRITE)
                  (#const MAP_ANONYMOUS | MAP_PRIVATE) (-1) 0

freeArena :: Ptr a -> CSize -> IO ()
freeArena = munmap

unlockArena :: Ptr a -> CSize -> IO ()
unlockArena ptr buf_size = () <$ mprotect ptr buf_size (#const PROT_READ | PROT_EXEC)

