{-# LANGUAGE CPP #-}
module Development.Lightening.Registers where

import Development.Lightening.Operand

jit_R0, jit_R1, jit_R2, jit_V0, jit_V1, jit_V2, jit_LR :: JitGprT
jit_F0, jit_F1, jit_F2 :: JitFprT

#if i386_HOST_ARCH
jit_R0 = JitGprT 0
jit_R1 = JitGprT 1
jit_R2 = JitGprT 2

jit_V0 = JitGprT 5
jit_V1 = JitGprT 6
jit_V2 = JitGprT 7

jit_F0 = JitFprT 0
jit_F1 = JitFprT 1
jit_F2 = JitFprT 2

jit_LR = JitGprT 3

#elif x86_64_HOST_ARCH
jit_R0 = JitGprT 0
jit_R1 = JitGprT 1
jit_R2 = JitGprT 2

jit_V0 = JitGprT 3
jit_V1 = JitGprT 12
jit_V2 = JitGprT 13

jit_F0 = JitFprT 0
jit_F1 = JitFprT 1
jit_F2 = JitFprT 2

jit_LR = JitGprT 11

{- elif mips_HOST_ARCH
  include "lightening/mips.h" -}

#elif arm_HOST_ARCH
jit_R0 = JitGprT 0
jit_R1 = JitGprT 1
jit_R2 = JitGprT 2

jit_V0 = JitGprT 4
jit_V1 = JitGprT 5
jit_V2 = JitGprT 6

jit_F0 = JitFprT 0
jit_F1 = JitFprT 2
jit_F2 = JitFprT 4

jit_LR = JitGprT 14

{- elif ppc_HOST_ARCH
  include "lightening/ppc.h" -}

#elif aarch64_HOST_ARCH
jit_R0 = JitGprT 0
jit_R1 = JitGprT 1
jit_R2 = JitGprT 2

jit_V0 = JitGprT 19
jit_V1 = JitGprT 20
jit_V2 = JitGprT 21

jit_F0 = JitFprT 0
jit_F1 = JitFprT 1
jit_F2 = JitFprT 2

jit_LR = JitGprT 30

{- elif s390_HOST_ARCH
  include "lightening/s390.h" -}

#else
#error "this architecture is not supported"
#endif

