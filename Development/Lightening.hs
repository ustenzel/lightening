{-# LANGUAGE ForeignFunctionInterface, CPP #-}
module Development.Lightening
    ( init_jit
    , jit_new_state
    , jit_destroy_state

    , jit_begin
    , jit_end
    , enter_jit_abi
    , leave_jit_abi
    , load_args
    , end
    , jit_enter_jit_abi
    , jit_leave_jit_abi
    , jit_load_args
    , jit_calli
    , jit_callr
    , jit_patch_here
    , jit_patch_there
    , patch_here
    , patch_there
    , jit_address
    , jit_address_to_function_pointer

    , jit_ldr
    , jit_ldi
    , jit_ldxr
    , jit_ldxi
    , jit_str
    , jit_sti
    , jit_stxr
    , jit_stxi
    , jit_retval
    , jit_bswapr
    , jit_truncr_d
    , jit_truncr_f

    , module Development.Lightening.Arena
    , module Development.Lightening.Instructions
    , module Development.Lightening.Jit
    , module Development.Lightening.Operand
    , module Development.Lightening.Registers

    , Word8
    , module Foreign.C.Types
    , module Foreign.Ptr
    ) where

#include "MachDeps.h"

import Control.Monad
import Data.Word            ( Word8 )
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Ptr
import Foreign.Storable

import Development.Lightening.Arena
import Development.Lightening.Instructions
import Development.Lightening.Jit
import Development.Lightening.Operand
import Development.Lightening.Registers

foreign import ccall unsafe "lightening.h init_jit"
    init_jit :: IO CInt

-- allocFn and freeFn not supported for now
jit_new_state :: IO JitState
jit_new_state = c_jit_new_state nullFunPtr nullFunPtr

foreign import ccall unsafe "lightening.h jit_new_state"
    c_jit_new_state :: FunPtr (CSize -> IO (Ptr a))
                    -> FunPtr (Ptr b -> IO ())
                    -> IO JitState

foreign  import ccall unsafe "lightening.h jit_destroy_state"
    jit_destroy_state :: JitState -> IO ()

foreign import ccall unsafe "lightening.h jit_begin"
    jit_begin :: JitState -> Ptr Word8 -> CSize -> IO ()

jit_end :: JitState -> (FunPtr f -> f) -> IO (f, CSize)
jit_end j f = alloca $ \psize -> liftM2 (,) (f `liftM` c_jit_end j psize) (peek psize)

end :: (FunPtr f -> f) -> Jit f
end f = Jit $ \k j -> alloca $ \psize -> c_jit_end j psize >>= flip k j . f

foreign import ccall unsafe "lightening.h jit_end"
    c_jit_end :: JitState -> Ptr CSize -> IO (FunPtr f)

foreign import ccall unsafe "lightening.h jit_enter_jit_abi"
    jit_enter_jit_abi :: JitState -> CSize -> CSize -> CSize -> IO CSize

enter_jit_abi :: CSize -> CSize -> CSize -> Jit CSize
enter_jit_abi x y z = Jit $ \k j -> jit_enter_jit_abi j x y z >>= flip k j

foreign import ccall unsafe "lightening.h jit_leave_jit_abi"
    jit_leave_jit_abi :: JitState -> CSize -> CSize -> CSize -> IO ()

leave_jit_abi :: CSize -> CSize -> CSize -> Jit ()
leave_jit_abi x y z = Jit $ \k j -> jit_leave_jit_abi j x y z >> k () j

jit_load_args :: JitState -> [JitOperand] -> IO ()
jit_load_args jit args =
    withArray args $ \pargs ->
        c_jit_load_args jit (fromIntegral $ length args) pargs

load_args :: [JitOperand] -> Jit ()
load_args args =
    Jit $ \k jit -> do
        withArray args (c_jit_load_args jit (fromIntegral $ length args))
        k () jit

foreign import ccall unsafe "lightening.h jit_load_args"
    c_jit_load_args :: JitState -> CSize -> Ptr JitOperand -> IO ()

jit_calli :: JitState -> FunPtr a -> [JitOperand] -> IO ()
jit_calli jit fun args =
    withArray args $ \pargs ->
        c_jit_calli jit fun (fromIntegral $ length args) pargs

foreign import ccall unsafe "lightening.h jit_calli"
    c_jit_calli :: JitState -> FunPtr a -> CSize -> Ptr JitOperand -> IO ()

jit_callr :: JitState -> JitGprT -> [JitOperand] -> IO ()
jit_callr jit reg args =
    withArray args $ \pargs ->
        c_jit_callr jit reg (fromIntegral $ length args) pargs

foreign import ccall unsafe "lightening.h jit_callr"
    c_jit_callr :: JitState -> JitGprT -> CSize -> Ptr JitOperand -> IO ()

foreign import ccall unsafe "lightening.h wrap_jit_patch_here"
    jit_patch_here :: JitState -> JitReloc -> IO ()

patch_here :: JitReloc -> Jit ()
patch_here r = Jit $ \k j -> jit_patch_here j r >> k () j

foreign import ccall unsafe "lightening.h wrap_jit_patch_there"
    jit_patch_there :: JitState -> JitReloc -> Ptr a -> IO ()

patch_there :: JitReloc -> Ptr a -> Jit ()
patch_there r p = Jit $ \k j -> jit_patch_there j r p >> k () j

foreign import ccall unsafe "lightening.h jit_address"
    jit_address :: JitState -> IO (Ptr a)

foreign import ccall unsafe "lightening.h jit_address_to_function_pointer"
    jit_address_to_function_pointer :: Ptr a -> IO (FunPtr a)


jit_ldr :: JitState -> JitGprT -> JitGprT -> IO ()
jit_ldi :: JitState -> JitGprT -> Ptr a -> IO ()
jit_ldxr :: JitState -> JitGprT -> JitGprT -> JitGprT -> IO ()
jit_ldxi :: JitState -> JitGprT -> JitGprT -> JitOffT -> IO ()
jit_str :: JitState -> JitGprT -> JitGprT -> IO ()
jit_sti :: JitState -> Ptr a -> JitGprT -> IO ()
jit_stxr :: JitState -> JitGprT -> JitGprT -> JitGprT -> IO ()
jit_stxi :: JitState -> JitOffT -> JitGprT -> JitGprT -> IO ()
jit_retval :: JitState -> JitGprT -> IO ()
jit_bswapr :: JitState -> JitGprT -> JitGprT -> IO ()
jit_truncr_d :: JitState -> JitGprT -> JitFprT -> IO ()
jit_truncr_f :: JitState -> JitGprT -> JitFprT -> IO ()


#if WORD_SIZE_IN_BITS == 32
jit_ldr = jit_ldr_i
jit_ldi = jit_ldi_i
jit_ldxr = jit_ldxr_i
jit_ldxi = jit_ldxi_i
jit_str = jit_str_i
jit_sti = jit_sti_i
jit_stxr = jit_stxr_i
jit_stxi = jit_stxi_i
jit_retval = jit_retval_i
jit_bswapr = jit_bswapr_ui
jit_truncr_d = jit_truncr_d_i
jit_truncr_f = jit_truncr_f_i
#else
jit_ldr = jit_ldr_l
jit_ldi = jit_ldi_l
jit_ldxr = jit_ldxr_l
jit_ldxi = jit_ldxi_l
jit_str = jit_str_l
jit_sti = jit_sti_l
jit_stxr = jit_stxr_l
jit_stxi = jit_stxi_l
jit_retval = jit_retval_l
jit_bswapr = jit_bswapr_ul
jit_truncr_d = jit_truncr_d_l
jit_truncr_f = jit_truncr_f_l
#endif

